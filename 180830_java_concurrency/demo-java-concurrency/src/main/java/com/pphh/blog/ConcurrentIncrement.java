package com.pphh.blog;

/**
 * A common interface to test concurrent operation with mutiple threads
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public interface ConcurrentIncrement {

    public void increment();

    public int getCount();

}
