package com.pphh.blog.block;

import com.pphh.blog.ConcurrentIncrement;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class declares a Atomic Integer state. It's thread safe.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class AtomicStateClass implements ConcurrentIncrement {

    private AtomicInteger i = new AtomicInteger(0);

    @Override
    public void increment() {
        i.incrementAndGet();
    }

    @Override
    public int getCount() {
        return i.get();
    }
}
