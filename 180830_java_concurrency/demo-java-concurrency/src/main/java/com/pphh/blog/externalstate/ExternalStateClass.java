package com.pphh.blog.externalstate;

import com.pphh.blog.block.IntegerNumber;

import java.util.List;

/**
 * External State Demo
 * All external states are not thread safe.
 *
 * @author huangyinhuang
 * @date 9/3/2018
 */
public class ExternalStateClass {

    public void iterate(List<IntegerNumber> numbers) {
        Integer size = numbers.size();
        for (int i = 0; i < size; i++) {
            System.out.println(numbers.get(i));
        }
    }

    public void iterate2(ThreadLocal<List<IntegerNumber>> numbersThreadLocal) {
        List<IntegerNumber> personList = numbersThreadLocal.get();
        Integer size = personList.size();
        for (int i = 0; i < size; i++) {
            System.out.println(personList.get(i));
        }
    }

    public void iterate2(final List<IntegerNumber> numbers) {
        Integer size = numbers.size();
        for (int i = 0; i < size; i++) {
            System.out.println(numbers.get(i));
        }
    }

}
