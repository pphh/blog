package com.pphh.blog.privatestate;

import com.pphh.blog.ConcurrentIncrement;
import com.pphh.blog.util.Logger;

/**
 * This class use ThreadLocal to hold a state, an Integer variable. It's Thread Safe.
 * The ThreadLocal separate the state among visiting threads.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class PrivateStateClass implements ConcurrentIncrement {

    private ThreadLocal<Integer> i = new ThreadLocal<>();

    public PrivateStateClass() {
        i.set(0);
    }

    @Override
    public void increment() {
        Integer value = i.get();
        i.set(value + 1);
        Logger.info(i.get().toString());
    }

    @Override
    public int getCount() {
        return i.get();
    }

}
