package com.pphh.blog.state;

import com.pphh.blog.ConcurrentIncrement;
import com.pphh.blog.util.Logger;
import com.pphh.blog.util.ThreadSafe;

/**
 * This class declare no fields, which make it stateless. A stateless class is Thread Safe.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
@ThreadSafe
public class StatelessClass implements ConcurrentIncrement {

    @Override
    public void increment() {
        int i = 0;
        String msg = String.valueOf(i++);
        Logger.info(msg);
    }

    @Override
    public int getCount() {
        return -1;
    }

}
