package com.pphh.blog.deadlock;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class Account {

    private Integer balance;

    public Account(Integer balance) {
        this.balance = balance;
    }

    public void debit(Integer amount) {
        balance = balance - amount;
    }

    public void credit(Integer amount) {
        balance = balance + amount;
    }

    public Integer getBalance() {
        return balance;
    }

}
