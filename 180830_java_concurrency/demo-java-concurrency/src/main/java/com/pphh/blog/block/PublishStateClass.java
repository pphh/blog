package com.pphh.blog.block;

import com.pphh.blog.ConcurrentIncrement;

/**
 * The class declares a number variable.
 * The number state is published out by getNumber() method, which makes it out of the class protects with thread synchronized lock.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class PublishStateClass implements ConcurrentIncrement {

    private IntegerNumber number = new IntegerNumber();

    @Override
    public synchronized void increment() {
        number.increment();
    }

    @Override
    public synchronized int getCount() {
        return number.getCount();
    }

    /**
     * note: the number state is published here, which will cause concurrent issue with multiple thread
     *
     * @return number state
     */
    public IntegerNumber getNumber() {
        return number;
    }

}
