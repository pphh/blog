package com.pphh.blog.finalstate;

import com.pphh.blog.ConcurrentIncrement;

/**
 * This class defines a constant state. It's Thread Safe. The constant state is shared among all visiting threads.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class FinalStateClass implements ConcurrentIncrement {

    private final Integer constant = 0;

    @Override
    public void increment() {
    }

    @Override
    public int getCount() {
        return constant;
    }

}
