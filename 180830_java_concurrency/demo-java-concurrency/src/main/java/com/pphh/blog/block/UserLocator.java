package com.pphh.blog.block;

import java.util.HashMap;
import java.util.Map;

/**
 * demo how to reduce the lock's scope
 *
 * @author huangyinhuang
 * @date 9/10/2018
 */
public class UserLocator {

    private final Map<String, String> userLocations = new HashMap<>();

    /**
     * position user in a thread safe way
     *
     * @param name     user name
     * @param position user's position
     * @return true if positioned
     */
    public synchronized boolean hasPositioned(String name, String position) {
        String key = String.format("%s.location", name);
        String location = userLocations.get(key);
        return location != null && position.equals(location);
    }

    /**
     * a better way to use the lock , reduce the holding time of this lock
     *
     * @param name     user name
     * @param position user's position
     * @return true if positioned
     */
    public boolean hasPositioned2(String name, String position) {
        String key = String.format("%s.location", name);
        String location;
        synchronized (this) {
            location = userLocations.get(key);
        }
        return location != null && position.equals(location);
    }

}
