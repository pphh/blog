package com.pphh.blog.deadlock;


/**
 * A demo of lock-ordering dead lock issue
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class LockOrderingDeadLock {

    /**
     * risk of dead lock, please see unit test case for how to repeat the dead lock issue
     *
     * @param from   from
     * @param to     to
     * @param amount amount
     * @throws Exception
     */
    public void transferMoney(Account from, Account to, Integer amount) throws Exception {
        synchronized (from) {
            synchronized (to) {
                if (from.getBalance() < amount) {
                    throw new Exception("insufficient balance in the account");
                } else {
                    from.debit(amount);
                    to.credit(amount);
                }
            }
        }
    }

}
