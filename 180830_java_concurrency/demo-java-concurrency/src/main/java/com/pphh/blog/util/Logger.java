package com.pphh.blog.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by huangyinhuang on 11/23/2017.
 */
public class Logger {

    public static void info(String msg) {
        String threadName = Thread.currentThread().getName();

        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);

        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }

}
