package com.pphh.blog.block;

import com.pphh.blog.util.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * The class declares a list of strings variable.
 * The personList is declared as public state, which makes it out of the class protects with thread synchronized lock.
 *
 * @author huangyinhuang
 * @date 8/31/2018
 */
public class PublicStateClass {

    public ArrayList<String> personList = new ArrayList<>();

    public synchronized void insert(String person) {
        personList.add(person);
    }

    public synchronized void iterate() {
        Integer size = personList.size();
        for (int i = 0; i < size; i++) {
            Logger.info(personList.get(i));
        }
    }

    /**
     * a recommended way to publish state
     *
     * @return a clone copy of person list
     */
    public List getList() {
        return (List) personList.clone();
    }

}
