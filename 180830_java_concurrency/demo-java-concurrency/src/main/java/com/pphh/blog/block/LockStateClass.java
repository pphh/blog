package com.pphh.blog.block;

import com.pphh.blog.ConcurrentIncrement;

/**
 * This class declares a Integer state.
 * All of class methods are protected by thread synchronized lock, which make the class's state ThreadSafe.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class LockStateClass implements ConcurrentIncrement {

    private Integer i = 0;

    @Override
    public synchronized void increment() {
        i++;
    }

    @Override
    public synchronized int getCount() {
        return i;
    }

}
