package com.pphh.blog.state;

import com.pphh.blog.ConcurrentIncrement;

/**
 * This class declares one fields: int i, which make it stateful. It's NOT Thread Safe.
 * A stateful variable should be protected from multiple thread visit, such as using Atomic state or using thread lock.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class StatefullClass implements ConcurrentIncrement {

    private int i = 0;

    @Override
    public void increment() {
        i++;
    }

    @Override
    public int getCount() {
        return i;
    }

}
