package com.pphh.blog.block;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class IntegerNumber {

    private Integer i = 0;

    public void increment() {
        i++;
    }

    public int getCount() {
        return i;
    }
}
