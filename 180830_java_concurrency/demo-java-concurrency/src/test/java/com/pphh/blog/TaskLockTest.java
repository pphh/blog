package com.pphh.blog;

import com.pphh.blog.task.BarrierTask;
import com.pphh.blog.task.CountDownLatchTask;
import com.pphh.blog.task.SemaphoreTask;
import com.pphh.blog.util.Logger;
import com.pphh.blog.util.ThreadPool;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 9/3/2018
 */
public class TaskLockTest {

    @Test
    public void testBarrierTask() throws InterruptedException {
        Logger.info("--  start of testBarrierTask  --");

        CyclicBarrier barrier = new CyclicBarrier(4, new Runnable() {
            @Override
            public void run() {
                Logger.info("barrier is ready, please go go go...");
            }
        });

        for (int i = 0; i < 4; i++) {
            BarrierTask task = new BarrierTask(barrier);
            ThreadPool.getThreadPool().execute(task);
        }

        // wait for tasks to be completed
        do {
            Thread.sleep(100);
        } while (ThreadPool.getThreadPool().getActiveCount() != 0);

        Logger.info("-- end of testBarrierTask --");
    }


    @Test
    public void testSemaphoreTask() throws InterruptedException {
        Logger.info("--  start of testSemaphoreTask  --");

        final Semaphore semaphore = new Semaphore(1);

        for (int i=0; i<4; i++){
            SemaphoreTask task = new SemaphoreTask(semaphore);
            ThreadPool.getThreadPool().execute(task);
        }

        // wait for tasks to be completed
        do {
            Thread.sleep(10);
            Logger.info("active count = " + ThreadPool.getThreadPool().getActiveCount());
        } while (ThreadPool.getThreadPool().getActiveCount() != 0);

        Logger.info("-- end of testSemaphoreTask --");
    }

    @Test
    public void testCountDownLatchTask() throws InterruptedException {
        Logger.info("--  start of testCountDownLatchTask  --");

        CountDownLatch startSignal = new CountDownLatch(1);
        CountDownLatch endSignal = new CountDownLatch(4);

        for (int i=0; i<4; i++){
            CountDownLatchTask task = new CountDownLatchTask(startSignal, endSignal);
            //ThreadPool.getThreadPool().execute(task);
            new Thread(task).start();
        }

        Logger.info("wait for 5 seconds...");
        Thread.sleep(5000);
        startSignal.countDown();
        endSignal.await();

        Logger.info("-- end of testCountDownLatchTask --");
    }
}
