package com.pphh.blog.task;

import com.pphh.blog.util.Logger;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 9/3/2018
 */
public class BarrierTask implements Runnable {
    private final CyclicBarrier barrier;

    public BarrierTask(CyclicBarrier barrier) {
        this.barrier = barrier;
    }

    @Override
    public void run() {
        Logger.info("start of task");
        try {
            this.barrier.await();
            Logger.info("run...");
        } catch (BrokenBarrierException | InterruptedException e) {
            e.printStackTrace();
        }
        Logger.info("end of task");
    }
}
