package com.pphh.blog;

import com.pphh.blog.deadlock.Account;
import com.pphh.blog.deadlock.LockOrderingDeadLock;
import com.pphh.blog.util.Logger;
import com.pphh.blog.util.ThreadPool;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class DeadLockTest {

    /**
     * caution: risk of dead lock, which you have to stop by force
     * remove @ignore tag when you decide to run this case
     *
     * @throws InterruptedException
     */
    @Ignore
    @Test
    public void testDeadLock() throws InterruptedException {

        final LockOrderingDeadLock lockOrderingLock = new LockOrderingDeadLock();
        final Account from = new Account(1000 * 20);
        final Account to = new Account(1000 * 20);

        for (int i = 0; i < 20; i++) {

            // prepare 1000 threads to be run
            // caution: risk of dead lock
            for (int j = 0; j < 1000; j++) {
                Runnable task = new Runnable() {
                    public void run() {
                        try {
                            if (new Random().nextInt() % 2 == 0) {
                                lockOrderingLock.transferMoney(from, to, 1);
                            } else {
                                lockOrderingLock.transferMoney(to, from, 1);
                            }
                        } catch (Exception ignore) {
                        }
                    }
                };
                ThreadPool.getThreadPool().execute(task);
            }

            // wait for thread tasks to be completed
            do {
                Thread.sleep(1000);
                Logger.info("[" + i + "] active task: " + ThreadPool.getThreadPool().getActiveCount());
            } while (ThreadPool.getThreadPool().getActiveCount() != 0);
        }
    }

}
