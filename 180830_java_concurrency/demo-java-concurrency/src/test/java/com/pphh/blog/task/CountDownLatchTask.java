package com.pphh.blog.task;

import com.pphh.blog.util.Logger;

import java.util.concurrent.CountDownLatch;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 9/3/2018
 */
public class CountDownLatchTask implements Runnable {

    private final CountDownLatch endSignal;
    private final CountDownLatch startSignal;

    public CountDownLatchTask(CountDownLatch startSignal, CountDownLatch endSignal) {
        this.startSignal = startSignal;
        this.endSignal = endSignal;
    }

    @Override
    public void run() {
        Logger.info("start of task");
        try {
            this.startSignal.await();
            Logger.info("run...");
            this.endSignal.countDown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Logger.info("end of task");
    }

}
