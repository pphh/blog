package com.pphh.blog.task;

import com.pphh.blog.util.Logger;

import java.util.concurrent.Semaphore;

/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 9/4/2018
 */
public class SemaphoreTask implements Runnable {
    private final Semaphore semaphore;

    public SemaphoreTask(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {

        try {
            this.semaphore.acquire();
            Logger.info("start of task");
            Logger.info("run...");
            Logger.info("end of task");
            this.semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
