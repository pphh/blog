package com.pphh.blog;

import com.pphh.blog.ConcurrentIncrement;
import com.pphh.blog.block.AtomicStateClass;
import com.pphh.blog.block.LockStateClass;
import com.pphh.blog.block.PublishStateClass;
import com.pphh.blog.block.IntegerNumber;
import com.pphh.blog.privatestate.PrivateStateClass;
import com.pphh.blog.state.StatefullClass;
import com.pphh.blog.util.Logger;
import com.pphh.blog.util.ThreadPool;
import junit.framework.Assert;
import org.junit.Test;


/**
 * Please add description here.
 *
 * @author huangyinhuang
 * @date 8/30/2018
 */
public class ConcurrentTest {

    /**
     * thread safe - atomic state
     *
     * @throws InterruptedException
     */
    @Test
    public void testAtomicStateClass() throws InterruptedException {
        // thread safe
        testConcurrent(new AtomicStateClass());
    }

    /**
     * thread safe - lock state
     *
     * @throws InterruptedException
     */
    @Test
    public void testLockStateClass() throws InterruptedException {
        // thread safe
        testConcurrent(new LockStateClass());
    }

    /**
     * thread unsafe - stateful
     *
     * @throws InterruptedException
     */
    @Test
    public void testStatefullClass() throws InterruptedException {
        // thread unsafe
        testConcurrent(new StatefullClass());
    }

    /**
     * thread unsafe - publish state
     *
     * @throws InterruptedException
     */
    @Test
    public void testPublishState() throws InterruptedException {
        final PublishStateClass concurrentService = new PublishStateClass();
        for (int i = 0; i < 10; i++) {

            // prepare 1000 threads to be run
            for (int j = 0; j < 1000; j++) {
                Runnable task = new Runnable() {
                    public void run() {
                        // note: the number state is published here, which cause concurrent issue with multiple thread
                        IntegerNumber i = concurrentService.getNumber();
                        i.increment();
                    }
                };
                ThreadPool.getThreadPool().execute(task);
            }

            // wait for thread tasks to be completed
            do {
                Thread.sleep(1000);
                Logger.info("[round " + i + "]active task: " + ThreadPool.getThreadPool().getActiveCount());
            } while (ThreadPool.getThreadPool().getActiveCount() != 0);

            // do check
            Logger.info("check the increment count, which should be equal to " + (i + 1) * 1000);
            Assert.assertEquals((i + 1) * 1000, concurrentService.getCount());
        }
    }

    @Test
    public void testPrivateState(){
        ConcurrentIncrement concurrentService = new PrivateStateClass();
        concurrentService.increment();
        Assert.assertEquals(1, concurrentService.getCount());
    }

    private void testConcurrent(final ConcurrentIncrement concurrentService) throws InterruptedException {

        for (int i = 0; i < 20; i++) {

            // prepare 1000 threads to be run
            for (int j = 0; j < 1000; j++) {
                Runnable task = new Runnable() {
                    public void run() {
                        concurrentService.increment();
                    }
                };
                ThreadPool.getThreadPool().execute(task);
            }

            // wait for thread tasks to be completed
            do {
                Thread.sleep(1000);
                Logger.info("[round " + i + "]active task: " + ThreadPool.getThreadPool().getActiveCount());
            } while (ThreadPool.getThreadPool().getActiveCount() != 0);

            // do check
            Logger.info("check the increment count, which should be equal to " + (i + 1) * 1000);
            Assert.assertEquals((i + 1) * 1000, concurrentService.getCount());
        }

    }


}
