
## 简介
演示java类各种状态和多线程并发的关系，包括，

  - 无状态类
  - 有状态类
  - 私有状态类
  - 不变状态类（常量状态）
  - 状态阻塞访问（加锁）
  - 状态非阻塞访问（原子状态）
  - 顺序锁死锁
  - 外部状态类

详细介绍见博文[《从类状态Java安全并发》](http://www.hyhblog.cn/2018/09/03/java_safe_concurrency)。

## 项目构建

整个项目目录结构如下，

```
- README 使用说明文件
- pom.xml 整个Maven项目的pom文件，这是演示项目的根目录
+ src/main
  + java/com/pphh/blog
    - ConcurrenIncrement 并发接口（自增）
    + state
      - StatelessClass 无状态类（并发安全）
      - StatefullClass 有状态类（并发不安全）
    + privatestate
      - PrivateStateClass 私有状态类（并发安全）
    + finalstate
      - FinalStateClass 不变状态类（并发安全）
    + block
      - LockStateClass 状态阻塞访问，通过加锁（并发安全）
      - AtomicStateClass 状态非阻塞访问，通过原子类（并发安全）
      - PublicStateClass 公开状态类（并发不安全）
      - PublishStateClass 公开状态类（并发不安全）
    + deadlock
      - LockOrderingDeadLock 顺序锁死锁
    + externalstate
      - ExternalStateClass 外部状态类（并发不安全）
+ src/test
  + java/com/pphh/blog
    - ConcurrentTest 并发测试
    - DeadLockTest 死锁测试
    - TaskLockTest 并发锁测试（闭锁latch、信号量semaphore、栅栏barrier）
```

本项目使用maven 3+进行构建，命令为，
```
mvn clean compile
```


## 演示
1. 进入单元测试目录src/test/java/com/pphh/blog

2. 运行单元测试类ConcurrentTest中的测试用例
   * testAtomicStateClass 测试原子状态类的并发，其并发安全，将运行成功
   * testLockStateClass 测试加锁状态类的并发，其并发安全，将运行成功
   * testStatefullClass 测试有状态类的并发，其并发不安全，将失败结束。
   运行日志样例如下，失败在第二轮并发。注：每次执行测试用例，失败的轮次可能都不一样。

   ```
   [20180903 15:55:14-327][main] [round 0]active task: 0
   [20180903 15:55:14-349][main] check the increment count, which should be equal to 1000
   [20180903 15:55:15-351][main] [round 1]active task: 0
   [20180903 15:55:15-351][main] check the increment count, which should be equal to 2000

   junit.framework.AssertionFailedError:
   Expected :2000
   Actual   :1995
   ```

   * testPublishState 测试公开状态类的并发，其并发不安全，将失败结束。
   运行日志样例如下，失败在第一轮并发。注：每次执行测试用例，失败的轮次可能都不一样。

   ```
   [20180903 15:55:56-407][main] [round 0]active task: 0
   [20180903 15:55:56-407][main] check the increment count, which should be equal to 1000

   junit.framework.AssertionFailedError:
   Expected :1000
   Actual   :565
   ```

   * testPrivateState 测试私有状态类的并发，其并发安全，将运行成功

3. 运行单元测试类TaskLockTest中的测试用例
   * testBarrierTask 通过栅栏barrier实现任务的同时并发，执行日志见如下，

   ```
   [20180904 10:25:27-012][main] --  start of testBarrierTask  --
   [20180904 10:25:27-028][pool-1-thread-2] start of task
   [20180904 10:25:27-028][pool-1-thread-1] start of task
   [20180904 10:25:27-028][pool-1-thread-3] start of task
   [20180904 10:25:27-029][pool-1-thread-4] start of task
   [20180904 10:25:27-029][pool-1-thread-4] barrier is ready, please go go go...
   [20180904 10:25:27-029][pool-1-thread-2] run...
   [20180904 10:25:27-029][pool-1-thread-2] end of task
   [20180904 10:25:27-029][pool-1-thread-4] run...
   [20180904 10:25:27-030][pool-1-thread-4] end of task
   [20180904 10:25:27-030][pool-1-thread-3] run...
   [20180904 10:25:27-030][pool-1-thread-3] end of task
   [20180904 10:25:27-030][pool-1-thread-1] run...
   [20180904 10:25:27-031][pool-1-thread-1] end of task
   [20180904 10:25:27-130][main] -- end of testBarrierTask --
   ```

   * testSemaphoreTask 通过信号量semaphore实现任务的按需并发，执行日志见如下，

   ```
   [20180904 10:27:37-880][main] --  start of testSemaphoreTask  --
   [20180904 10:27:37-884][pool-1-thread-1] start of task
   [20180904 10:27:37-885][pool-1-thread-1] run...
   [20180904 10:27:37-885][pool-1-thread-1] end of task
   [20180904 10:27:37-885][pool-1-thread-2] start of task
   [20180904 10:27:37-885][pool-1-thread-2] run...
   [20180904 10:27:37-885][pool-1-thread-2] end of task
   [20180904 10:27:37-886][pool-1-thread-4] start of task
   [20180904 10:27:37-886][pool-1-thread-4] run...
   [20180904 10:27:37-886][pool-1-thread-4] end of task
   [20180904 10:27:37-887][pool-1-thread-3] start of task
   [20180904 10:27:37-887][pool-1-thread-3] run...
   [20180904 10:27:37-887][pool-1-thread-3] end of task
   [20180904 10:27:37-894][main] active count = 0
   [20180904 10:27:37-894][main] -- end of testSemaphoreTask --
   ```

   * testCountDownLatchTask  通过闭锁latch实现任务的同时并发，执行日志见如下，

   ```
   [20180904 10:27:32-863][main] --  start of testCountDownLatchTask  --
   [20180904 10:27:32-879][main] wait for 5 seconds...
   [20180904 10:27:32-880][Thread-0] start of task
   [20180904 10:27:32-880][Thread-1] start of task
   [20180904 10:27:32-880][Thread-3] start of task
   [20180904 10:27:32-881][Thread-2] start of task
   [20180904 10:27:37-879][Thread-3] run...
   [20180904 10:27:37-879][Thread-2] run...
   [20180904 10:27:37-879][Thread-3] end of task
   [20180904 10:27:37-879][Thread-2] end of task
   [20180904 10:27:37-879][Thread-0] run...
   [20180904 10:27:37-879][Thread-1] run...
   [20180904 10:27:37-879][Thread-0] end of task
   [20180904 10:27:37-879][Thread-1] end of task
   [20180904 10:27:37-879][main] -- end of testCountDownLatchTask --
   ```
