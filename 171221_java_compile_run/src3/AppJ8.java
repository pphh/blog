import java.util.stream.IntStream;

public class AppJ8 {
    
    public static void main(String[] args){
        System.out.println("Hello,world");
        System.out.println("This app use JDK 8 feature (Lambdas, type annotations etc), which should be compiled with JDK 8+.");
        
        // use stream and lambda to calc the sum of [0-9]
        Integer sum = IntStream.range(0,10)
                .filter(n -> n%2 == 0)
                .reduce(0, (x, y) -> x+y);
        
        System.out.println("sum: " + sum);
    }
    
}