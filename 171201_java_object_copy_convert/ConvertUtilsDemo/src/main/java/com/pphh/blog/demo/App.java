package com.pphh.blog.demo;

import com.pphh.blog.demo.beanutils.ApacheCommonUtils;
import com.pphh.blog.demo.beanutils.SpringUtils;
import com.pphh.blog.demo.demo.Demo1;
import com.pphh.blog.demo.demo.Demo2;
import com.pphh.blog.demo.demo.Demo3;
import com.pphh.blog.demo.demo.Demo4;
import com.pphh.blog.demo.shared.Logger;

/**
 * Created by pphh on 11/30/2017.
 */
public class App {

    public static void main(String[] args) {
        Logger.logMessage("演示：使用org.apache.commons.beanutils.BeanUtils进行复制");
        ApacheCommonUtils.copyObject();

        Logger.logMessage("演示：使用org.springframework.beans.BeanUtils进行复制");
        SpringUtils.copyObject();
        SpringUtils.copyObjectWithIgnore();

        Logger.logMessage("演示：使用ConvertUtils进行转换单个实体");
        Demo1.copyObject();

        Logger.logMessage("演示：使用ConvertUtils转换列表，列表中的对象转换成新类型");
        Demo2.copyList();

        Logger.logMessage("演示：使用ConvertUtils转换列表，使用自定义mapper");
        Demo2.copyListWithMapper();

        Logger.logMessage("演示：使用ConvertUtils转换列表类型为ArrayList，HashSet， LinkedList");
        Demo3.copyAsNewTypeList();
        Demo3.copyAsNewTypeList2();

        Logger.logMessage("演示：使用ConvertUtils转换列表，同时将列表类型和集合中的对象一起转换");
        Demo3.copyAsNewTypeObjectAndList();

        Logger.logMessage("演示：使用ConvertUtils转换Map，一个简单将列表转换为Map");
        Demo4.copyListToMap();

        Logger.logMessage("演示：使用ConvertUtils将列表类型和Map相互转换");
        Demo4.copyBetweenListAndMap();

        Logger.logMessage("演示：使用ConvertUtils将Map转换为其它类型Map：HashMap、LinkedHashMap、Hashtable");
        Demo4.copyMapToMap();
    }

}
