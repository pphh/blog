package com.pphh.blog.demo.beanutils;

import com.pphh.blog.demo.user.UserDao;
import com.pphh.blog.demo.user.UserEntity;
import com.pphh.blog.demo.user.UserVO;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by pphh on 11/30/2017.
 * Demo how to use apache common bean utils
 */
public class ApacheCommonUtils {

    public static void copyObject() {
        UserEntity user = UserDao.SingleInstance.findById(1L);
        UserVO userVO = new UserVO();
        try {
            BeanUtils.copyProperties(userVO, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
