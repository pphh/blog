package com.pphh.blog.demo.beanutils;

import com.pphh.blog.demo.user.UserDao;
import com.pphh.blog.demo.user.UserEntity;
import com.pphh.blog.demo.user.UserVO;
import org.springframework.beans.BeanUtils;


/**
 * Created by pphh on 11/30/2017.
 * Demo how to use spring bean utils
 */
public class SpringUtils {

    public static void copyObject() {
        UserEntity user = UserDao.SingleInstance.findById(1L);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
    }

    public static void copyObjectWithIgnore() {
        UserEntity user = UserDao.SingleInstance.findById(1L);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO, "id");
    }

}
