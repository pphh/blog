package com.pphh.blog.demo.demo;


import com.pphh.blog.demo.ConvertUtils;
import com.pphh.blog.demo.user.UserDao;
import com.pphh.blog.demo.user.UserEntity;
import com.pphh.blog.demo.user.UserVO;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by pphh on 11/30/2017.
 * 使用ConvertUtils转换列表(Collection)集合，列表类型和列表中的对象转换成新类型
 */
public class Demo3 {

    /**
     * 转换列表类型为ArrayList，HashSet， LinkedList
     */
    public static void copyAsNewTypeList() {
        Iterable<Integer> iterable = IntStream.range(0, 5).boxed().collect(Collectors.toList());
        ArrayList<Integer> arrayList = (ArrayList<Integer>) ConvertUtils.collect(iterable, ArrayList::new);
        HashSet<Integer> hashSet = (HashSet<Integer>) ConvertUtils.collect(iterable, HashSet::new);
        LinkedList<Integer> linkedList = (LinkedList<Integer>) ConvertUtils.collect(iterable, LinkedList::new);
    }

    /**
     * 转换列表类型为ArrayList，HashSet， LinkedList
     */
    public static void copyAsNewTypeList2() {
        Iterable<UserEntity> users = UserDao.SingleInstance.findAll();
        ArrayList<UserEntity> arrayList = (ArrayList<UserEntity>) ConvertUtils.collect(users, ArrayList::new);
        HashSet<UserEntity> hashSet = (HashSet<UserEntity>) ConvertUtils.collect(users, HashSet::new);
        LinkedList<UserEntity> linkedList = (LinkedList<UserEntity>) ConvertUtils.collect(users, LinkedList::new);
    }

    /**
     * 转换列表类型的同时，也把集合中的对象更换为新类型
     */
    public static void copyAsNewTypeObjectAndList() {
        Iterable<UserEntity> users = UserDao.SingleInstance.findAll();
        ArrayList<UserVO> arrayList = (ArrayList<UserVO>) ConvertUtils.collect(users, UserVO.class, ArrayList::new);
        HashSet<UserVO> hashSet = (HashSet<UserVO>) ConvertUtils.collect(users, UserVO.class, HashSet::new);
        LinkedList<UserVO> linkedList = (LinkedList<UserVO>) ConvertUtils.collect(users, UserVO.class, LinkedList::new);
    }

}
