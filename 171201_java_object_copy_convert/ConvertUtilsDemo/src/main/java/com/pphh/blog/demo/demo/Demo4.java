package com.pphh.blog.demo.demo;


import com.pphh.blog.demo.ConvertUtils;
import com.pphh.blog.demo.user.UserDao;
import com.pphh.blog.demo.user.UserEntity;
import com.pphh.blog.demo.user.UserVO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by pphh on 11/30/2017.
 * 使用ConvertUtils转换Map，将
 */
public class Demo4 {

    /**
     * 一个简单将列表转换为Map
     */
    public static void copyListToMap() {
        List<UserEntity> users = UserDao.SingleInstance.findAll();
        Map<String, UserEntity> result = users.stream().collect(Collectors.toMap(UserEntity::getName, Function.identity()));
        Map<String, UserEntity> result2 = ConvertUtils.map(users, UserEntity::getName, Function.identity(), HashMap<String, UserEntity>::new);
    }

    /**
     * 使用ConvertUtils将列表转换为Map，然后又把Map转换成列表（ArrayList、HashSet、LinkedList）
     */
    public static void copyBetweenListAndMap() {
        List<UserEntity> users = UserDao.SingleInstance.findAll();
        Map<String, UserEntity> result = ConvertUtils.map(users, UserEntity::getName);

        Collection users2 = result.values();
        List<UserEntity> user3 = (List<UserEntity>) ConvertUtils.collect(result, ArrayList::new);
        Set<UserEntity> user4 = (Set<UserEntity>) ConvertUtils.collect(result, HashSet::new);
        List<UserEntity> user5 = (List<UserEntity>) ConvertUtils.collect(result, LinkedList::new);
    }

    /**
     * 使用ConvertUtils将Map转换为其它类型Map：HashMap、LinkedHashMap、Hashtable
     */
    public static void copyMapToMap() {
        List<UserEntity> users = UserDao.SingleInstance.findAll();
        Map<String, UserEntity> result = ConvertUtils.map(users, UserEntity::getName);
        Map<String, UserVO> newMap = ConvertUtils.map(result, UserVO.class, HashMap<String, UserVO>::new);
        Map<String, UserVO> newMap2 = ConvertUtils.map(result, UserVO.class, LinkedHashMap<String, UserVO>::new);
        Hashtable<String, UserVO> newMap3 = (Hashtable<String, UserVO>) ConvertUtils.map(result, UserVO.class, Hashtable<String, UserVO>::new);
    }

}
