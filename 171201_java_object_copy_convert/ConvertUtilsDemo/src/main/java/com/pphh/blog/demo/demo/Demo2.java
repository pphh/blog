package com.pphh.blog.demo.demo;

import com.pphh.blog.demo.ConvertUtils;
import com.pphh.blog.demo.user.UserDao;
import com.pphh.blog.demo.user.UserEntity;
import com.pphh.blog.demo.user.UserVO;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * Created by pphh on 11/30/2017.
 * 使用ConvertUtils转换列表，列表中的对象转换成新类型
 */
public class Demo2 {

    public static void copyList() {
        List<UserEntity> users = UserDao.SingleInstance.findAll();
        List<UserVO> userVOs = ConvertUtils.convert(users, UserVO.class);
    }

    public static void copyListWithMapper() {
        List<UserEntity> users = UserDao.SingleInstance.findAll();
        List<UserVO> userVOs = ConvertUtils.convert(users, Demo2::mapper);
    }

    private static UserVO mapper(UserEntity user) {
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        userVO.setDetails("Name=" + user.getName() + ", Email=" + user.getEmail());
        return userVO;
    }

}
