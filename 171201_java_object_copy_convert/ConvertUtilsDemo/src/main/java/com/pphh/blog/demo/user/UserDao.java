package com.pphh.blog.demo.user;

import java.util.*;

/**
 * Created by pphh on 11/30/2017.
 */
public class UserDao {

    public static UserDao SingleInstance = new UserDao();

    public UserEntity findById(Long id) {
        return build(id);
    }

    public List<UserEntity> findAll() {

        List<UserEntity> userList = new ArrayList<UserEntity>();

        for (long i = 0; i < 10; i++) {
            UserEntity user = build(i);
            userList.add(user);
        }

        return userList;
    }

    private UserEntity build(Long userId) {
        UserEntity user = new UserEntity();

        user.setId(userId);
        user.setName("user" + userId);
        user.setEmail("user" + userId + "@test.com");
        user.setInsertTime(new Date());
        user.setPassword(UUID.randomUUID().toString());

        return user;
    }

}
