package com.pphh.blog.demo.demo;

import com.pphh.blog.demo.ConvertUtils;
import com.pphh.blog.demo.user.UserDao;
import com.pphh.blog.demo.user.UserEntity;
import com.pphh.blog.demo.user.UserVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by pphh on 11/30/2017.
 * <p>
 * 一个简单的演示，使用ConvertUtils.convert复制对象
 */
public class Demo1 {

    public static void copyObjectWithNew() {
        UserEntity user = UserDao.SingleInstance.findById(1L);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
    }

    /**
     * 使用ConvertUtils来转换对象
     * 和上面copyObjectWithNew相比，少了一步new Object
     */
    public static void copyObject() {
        UserEntity user = UserDao.SingleInstance.findById(1L);
        UserVO userVO = ConvertUtils.convert(user, UserVO.class);
    }
}
