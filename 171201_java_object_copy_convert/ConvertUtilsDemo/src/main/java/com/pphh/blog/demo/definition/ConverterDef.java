package com.pphh.blog.demo.definition;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by pphh on 12/1/2017.
 * This is the interface definition of the ConvertUtils class
 */
public class ConverterDef {

    public static <S, T> T convert(S s, Class<T> tClass) {
        return null;
    }

    public static <S, T> T convernt(S s, Function<S, T> mapper) {
        return null;
    }

    public static <S, T> List<T> convert(Iterable<S> s, Class<T> tClass) {
        return null;
    }

    public static <S, T> List<T> convert(Iterable<S> s, Function<S, T> mapper) {
        return null;
    }

    public static <S, U extends Collection<S>> U collect(Iterable<S> s, CollectionFactory<S, U> collectionFactory) {
        return null;
    }

    public static <S, T, U extends Collection<T>> U collect(Iterable<S> s, Class<T> tClass, CollectionFactory<T, U> collectionFactory) {
        return null;
    }

    public static <S> Map<String, S> map(Iterable<S> s, Function<S, String> keyGenerator) {
        return null;
    }

    public static <K, S, U extends Collection<S>> U collect(Map<K, S> map, CollectionFactory<S, U> factory) {
        return null;
    }

    public static <K, V, T, U extends Map<K, T>> U map(Map<K, V> srcMap, Class<T> tClass, MapFactory<K, T, U> mapFactory) {
        return null;
    }

    public interface CollectionFactory<S, U extends Collection<S>> {
        U createCollection();
    }

    public interface MapFactory<K, T, U extends Map<K, T>> {
        U createMap();
    }
}
