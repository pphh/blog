package com.pphh.blog.demo;

import org.springframework.beans.BeanUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by pphh on 11/30/2017.
 * This utils class help to convert an object/a collection of objects into different class type.
 */
public class ConvertUtils {

    public static <S, T> T convert(S s, T t) {
        BeanUtils.copyProperties(s, t);
        return t;
    }

    public static <S, T> T convert(S s, Class<T> clazz) {
        T t = null;
        try {
            t = clazz.newInstance();
            BeanUtils.copyProperties(s, t);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public static <S, T> List<T> convert(Iterable<S> iterable, Class<T> clazz) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(s -> ConvertUtils.convert(s, clazz))
                .collect(Collectors.toList());
    }

    public static <S, T> List<T> convert(Iterable<S> iterable, Function<? super S, ? extends T> mapper) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(mapper)
                .collect(Collectors.toList());
    }

    public static <S, U extends Collection<S>> U collect(Iterable<S> iterable, CollectionFactory<S, U> factory) {
        U collection = factory.createCollection();
        iterable.forEach(collection::add);
        return collection;
    }

    public static <S, U extends Collection<T>, T> U collect(Iterable<S> iterable, Class<T> clazz, CollectionFactory<T, U> factory) {
        Iterable<T> list = convert(iterable, clazz);
        U collection = factory.createCollection();
        list.forEach(collection::add);
        return collection;
    }

    public static <K, S, U extends Collection<S>> U collect(Map<K, S> map, CollectionFactory<S, U> factory) {
        U collection = factory.createCollection();
        map.values().iterator().forEachRemaining(collection::add);
        return collection;
    }

    /**
     * convert iterable List<S> into Map<K, S>
     *
     * @param iterable     iterable object
     * @param keyGenerator a key generation function, which takes S object as input and generate K value
     * @param <S>          Source Object Class
     * @return Map
     */
    public static <S> Map<String, S> map(Iterable<S> iterable, Function<? super S, String> keyGenerator) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toMap(keyGenerator, Function.identity()));
    }

    /**
     * convert iterable List<S> into Map<K, V>
     *
     * @param iterable       iterable object
     * @param keyGenerator   a key generation function, which takes S object as input and generate K object as key
     * @param valueGenerator a value generation function, which takes S object as input and generate V object as value
     * @param factory        a fatory to create map object
     * @param <S>            Source Object Class
     * @param <K>            Key Object Class
     * @param <V>            Value Object Class
     * @param <U>            Map<K, V> object
     * @return Map
     */
    public static <S, K, V, U extends Map<K, V>> U map(Iterable<S> iterable,
                                                       Function<? super S, K> keyGenerator,
                                                       Function<? super S, V> valueGenerator,
                                                       MapFactory<K, V, U> factory) {
        U newMap = factory.createMap();
        iterable.forEach(entry -> newMap.put(keyGenerator.apply(entry), valueGenerator.apply(entry)));
        return newMap;
    }

    public static <K, S, T, U extends Map<K, T>> U map(Map<K, S> srcMap, Class<T> clazz, MapFactory<K, T, U> factory) {
        U newMap = factory.createMap();
        srcMap.entrySet().forEach(ksEntry -> newMap.put(ksEntry.getKey(), ConvertUtils.convert(ksEntry.getValue(), clazz)));
        return newMap;
    }

    public interface CollectionFactory<T, U extends Collection<T>> {
        U createCollection();
    }

    public interface MapFactory<K, T, U extends Map<K, T>> {
        U createMap();
    }

}
