package channel;

import util.Logger;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by huangyinhuang on 1/31/2018.
 */
public class ChannelClient {

    public static void main(String[] args) throws Exception {
        Logger.info("start to connect remote server.");
        InetSocketAddress remoteServerAddress = new InetSocketAddress("127.0.0.1", 9000);
        SocketChannel client = SocketChannel.open();
        client.configureBlocking(false);

        client.connect(remoteServerAddress);

        int count = 0;
        while (!client.finishConnect()) {
            System.out.print("waiting for server, count = " + count++);
            Thread.sleep(200);
        }

        Logger.printClientInfo(client);
        Logger.info("Is connected: " + client.isConnected());

        // send a message to server
        String msg = "Hello server, this is channel client.";
        ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
        client.write(buffer);

        client.close();
        System.out.print("the end.");
    }

}
