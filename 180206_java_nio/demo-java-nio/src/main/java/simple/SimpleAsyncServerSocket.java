package simple;

import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * Created by huangyinhuang on 2/5/2018.
 */
public class SimpleAsyncServerSocket {

    public static void main(String[] args) throws Exception {
        AsynchronousServerSocketChannel server = AsynchronousServerSocketChannel.open();
        server = server.bind(new InetSocketAddress(9000));

        OnAcceptCompletionHandler onAcceptCompletion = new OnAcceptCompletionHandler();
        server.accept(null, onAcceptCompletion);

        // app continue to run...
        Thread.sleep(100000);
        server.close();
    }

    public static class OnAcceptCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, Object> {

        @Override
        public void completed(AsynchronousSocketChannel srv, Object attachment) {
            System.out.println("Received a new client connection.");
            //srv.read(buffer...)
        }

        @Override
        public void failed(Throwable exc, Object attachment) {
            System.out.println("Received a failure message on client connection.");
        }
    }

}
