package simple;

import java.nio.IntBuffer;

/**
 * Created by huangyinhuang on 2/5/2018.
 * a simple demo of Java NIO Buffer
 */
public class SimpleBuffer {

    public static void main(String[] args) {
        // initialize the buffer
        IntBuffer buffer = IntBuffer.allocate(10);
        print(buffer);

        System.out.println("write 5 int number into buffer");
        for (int i = 0; i < 6; i++) {
            buffer.put(i);
            print(buffer);
        }

        // flip the buffer, which set the limit flag to the position of last write data
        System.out.println("switch write mode to read mode");
        buffer.flip();
        print(buffer);

        System.out.println("read 5 int number into buffer");
        for (int i = 0; i < 6; i++) {
            int n = buffer.get();
            System.out.println("read number: " + n);
            print(buffer);
        }

        // clear the buffer, which reset the limit/position
        System.out.println("clear the buffer's position");
        buffer.clear();
        print(buffer);
    }

    public static void print(IntBuffer buffer) {
        int[] array = buffer.array();
        StringBuilder builder = new StringBuilder();
        for (int n : array) {
            builder.append(n);
        }

        String info = String.format("capacity: %s, position: %s, limit: %s, array = %s",
                buffer.capacity(), buffer.position(), buffer.limit(), builder.toString());
        System.out.println(info);
    }
}
