package bio;

import util.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by huangyinhuang on 1/31/2018.
 */
public class BioClient {

    private Socket client;
    private PrintWriter printWriter;

    public BioClient() throws IOException, InterruptedException {

        while (true) {
            client = new Socket("127.0.0.1", 9000);
            Logger.info("a new client is connected with server port 9000");

            printWriter = new PrintWriter(client.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            Logger.info("please input any message here and press enter, then message will send to server.");
            printWriter.write(reader.readLine());
            printWriter.close();
            Thread.sleep(100);
        }

    }

    public static void main(String[] args) {
        Logger.info("client is starting...");

        try {
            Logger.info("client is trying to connect remote server...");
            new BioClient();
            Logger.info("client is completed with the communication.");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        Logger.info("the end.");
    }

}
