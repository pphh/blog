package bio;

import util.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by huangyinhuang on 1/31/2018.
 */
public class BioServer {

    private Socket socket;
    private ServerSocket server;

    public BioServer() throws IOException, InterruptedException {
        server = new ServerSocket(9000);
        Logger.info("server is started on port 9000");

        while (true) {
            // please note: server.accept() will block current thread
            Logger.info("server is waiting for client connection, current thread is blocking by accept() method");
            socket = server.accept();
            Logger.info("connected with a client.");

            InputStreamReader reader = new InputStreamReader(socket.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(reader);

            String msg = bufferedReader.readLine();
            Logger.info("Received message : " + msg);
            socket.shutdownInput();
            Thread.sleep(100);
        }

    }

    public static void main(String[] args) {
        Logger.info("server is starting...");

        try {
            new BioServer();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        Logger.info("the end.");
    }

}
