package blocking;

import util.Logger;

import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

/**
 * Created by huangyinhuang on 2/2/2018.
 */
public class PingClient {

    public static void main(String[] args) throws Exception {
        Logger.info("start to connect remote server.");
        InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 9000);
        SocketChannel client = SocketChannel.open();
        client.configureBlocking(true);

        client.connect(addr);
        int count = 0;
        while (!client.finishConnect()) {
            System.out.print("waiting for server, count = " + count++);
            Thread.sleep(200);
        }

        Logger.printClientInfo(client);
        Logger.info("Is connected: " + client.isConnected());

        client.close();
        System.out.print("the end.");
    }

}
