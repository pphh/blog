package blocking;

import util.Logger;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by huangyinhuang on 1/31/2018.
 */
public class NoneBlockingNioServer {

    public static void main(String[] args) throws Exception {
        // please note: configure channel blocking state as false
        ServerSocketChannel server = ServerSocketChannel.open();
        server.configureBlocking(false);

        Logger.info("A server is started on port 9000");
        ServerSocket serverSocket = server.socket();
        serverSocket.bind(new InetSocketAddress(9000));

        // please note: server.accept() will not block current thread
        // Accept method will return null directly if no client is connected
        SocketChannel channel = null;
        int count = 0;
        while (channel == null) {
            Logger.info("The server is trying to connect client, count=" + count++);
            channel = server.accept();
            Thread.sleep(500);
        }
        Logger.printClientInfo(channel);
        Logger.info("Is connected: " + channel.isConnected());
        channel.close();

        server.close();
        Logger.info("the end");
    }

}
