package blocking;

import util.Logger;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by huangyinhuang on 1/31/2018.
 */
public class BlockingNioServer {

    public static void main(String[] args) throws Exception {
        // please note: configure channel blocking state as true
        ServerSocketChannel server = ServerSocketChannel.open();
        server.configureBlocking(true);

        Logger.info("A server is started on port 9000");
        ServerSocket serverSocket = server.socket();
        serverSocket.bind(new InetSocketAddress(9000));

        // please note: server.accept() will block current thread
        Logger.info("Server is waiting for client connection, current thread is blocking...");
        SocketChannel channel = server.accept();
        Logger.printClientInfo(channel);
        Logger.info("Is connected: " + channel.isConnected());
        channel.close();

        server.close();
        Logger.info("the end");
    }

}
