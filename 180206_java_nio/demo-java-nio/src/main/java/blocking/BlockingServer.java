package blocking;

import util.Logger;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by huangyinhuang on 1/31/2018.
 */
public class BlockingServer {

    public static void main(String[] args) throws Exception {
        Logger.info("A server is started on port 9000");
        ServerSocket server = new ServerSocket(9000);

        // please note: server.accept() will block current thread
        Logger.info("The server wait for client connection.");
        Socket socket = server.accept();
        Logger.info("socket: " + socket);
        Logger.info("Is connected: " + socket.isConnected());

        socket.close();
        server.close();
        Logger.info("the end.");
    }

}
