package aio;

import util.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * Created by huangyinhuang on 2/1/2018.
 */
public class AioServer {

    private static AsynchronousServerSocketChannel server = null;

    public static void main(String[] args) throws Exception {
        Logger.info("start the server.");
        server = AsynchronousServerSocketChannel.open();
        server = server.bind(new InetSocketAddress(9000));

        // use a callback handler to handle new message upcoming
        // please note: server.accept() will not block current thread
        OnAcceptCompletionHandler onAcceptCompletion = new OnAcceptCompletionHandler();
        server.accept(null, onAcceptCompletion);

        while (true) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }

        server.close();
        Logger.info("the end.");

    }

    public static class OnAcceptCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, Object> {

        @Override
        public void completed(AsynchronousSocketChannel srv, Object attachment) {
            Logger.info("Received a new client connection.");

            ByteBuffer readBuffer = ByteBuffer.allocate(64);
            OnReadCompletionHandler onReadCompletion = new OnReadCompletionHandler(srv, readBuffer);
            try {
                // please note: server.read() will not block current thread
                srv.read(readBuffer, readBuffer, onReadCompletion);
            } catch (Exception e) {
                Logger.info("Received an exception when trying to read client msg buffer.");
            } finally {
                Logger.info("Prepare to accept another new client.");
                server.accept(null, this);
            }
        }

        @Override
        public void failed(Throwable exc, Object attachment) {
            Logger.info("Received a failure message on client connection.");
        }
    }

    public static class OnReadCompletionHandler implements CompletionHandler<Integer, ByteBuffer> {

        private AsynchronousSocketChannel srv;
        private ByteBuffer readBuffer;

        public OnReadCompletionHandler(AsynchronousSocketChannel srv, ByteBuffer readBuffer) {
            this.srv = srv;
            this.readBuffer = readBuffer;
        }

        @Override
        public void completed(Integer result, ByteBuffer buffer) {
            Logger.info("Received a new message from client.");

            if (result == -1) {

                Logger.info("Client has closed the connection, shutdown the channel.");
                try {
                    this.srv.close();
                } catch (IOException e) {
                    Logger.info("Receive an exception when trying to shutdown the channel.");
                }

            } else {
                buffer.flip();
                if (buffer.hasRemaining()) {
                    // print messages from client
                    byte[] msgBytes = new byte[buffer.remaining()];
                    buffer.get(msgBytes);
                    Logger.printNewMessage(new String(msgBytes).trim());

                    // send a reply to client
                    srv.write(ByteBuffer.wrap("Hello client, this is AIO Server. Your message is received.\r\n".getBytes()));
                }
                buffer.clear();

                // please note: server.read() will not current thread
                Logger.info("Client connection still running, continue the channel.");
                this.srv.read(readBuffer, readBuffer, this);
            }

        }

        @Override
        public void failed(Throwable exc, ByteBuffer buffer) {
            Logger.info("Received a error message from client. Shutdown the channel.");
            try {
                this.srv.close();
            } catch (IOException e) {
                Logger.info("Receive an exception when trying to shutdown the channel.");
            }
        }
    }
}
