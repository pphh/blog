package aio;

import util.Logger;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

/**
 * Created by huangyinhuang on 2/2/2018.
 */
public class SimpleClient {

    private static AsynchronousSocketChannel client = null;

    public static void main(String[] args) throws Exception {
        Logger.info("start to connect remote server.");
        client = AsynchronousSocketChannel.open();
        Future<Void> connectFuture = client.connect(new InetSocketAddress("127.0.0.1", 9000));

        while (!connectFuture.isDone()) {
            Logger.info("wait for connection to server.");
            Thread.sleep(1000);
        }

        // send a message to the server
        String msg = "Hello server, this is simple aio client!";
        ByteBuffer message = ByteBuffer.wrap(msg.getBytes());
        Logger.info("Sending message to the server...");
        client.write(message);

        client.close();
        Logger.info("the end.");
    }

}
