package aio;

import util.ChannelUtil;
import util.Logger;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * Created by huangyinhuang on 2/1/2018.
 */
public class AioClient {

    private static AsynchronousSocketChannel client = null;

    public static void main(String[] args) throws Exception {
        Logger.info("start to connect remote server.");
        client = AsynchronousSocketChannel.open();
        client.connect(new InetSocketAddress("127.0.0.1", 9000));

        String msg = "Hello server, this is AIO client! ";
        Logger.info("Sending message: " + msg);
        ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());

        // send a message
        OnWriteCompletionHandler onWriteCompletion = new OnWriteCompletionHandler(client);
        client.write(buffer, buffer, onWriteCompletion);

        // wait for reply after message sent-out
        //OnReadCompletionHandler onReadCompletion = new OnReadCompletionHandler(client);
        //client.read(buffer, buffer, onReadCompletion);

        while (true) {
            try {
                Thread.sleep(100);
                if (!client.isOpen()) {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }

        client.close();
        Logger.info("the end.");
    }

    public static class OnWriteCompletionHandler implements CompletionHandler<Integer, ByteBuffer> {

        private AsynchronousSocketChannel client;
        private OnReadCompletionHandler onReadCompletion;
        private ByteBuffer readBuffer;

        public OnWriteCompletionHandler(AsynchronousSocketChannel client) {
            this.client = client;
            this.onReadCompletion = new OnReadCompletionHandler(client);
            this.readBuffer = ByteBuffer.allocate(64);
        }

        @Override
        public void completed(Integer result, ByteBuffer buffer) {
            Logger.info("OnWriteCompletionHandler::completed");

            if (result == -1) {
                Logger.info("Server has closed the connection, shutdown the channel.");
                ChannelUtil.close(this.client);
            } else {
                buffer.flip();
                if (buffer.hasRemaining()) {
                    // print messages from server
                    byte[] msgBytes = new byte[buffer.remaining()];
                    buffer.get(msgBytes);
                    Logger.printNewMessage(new String(msgBytes).trim());
                }
                buffer.clear();

                // wait for reply after message sent-out
                this.client.read(this.readBuffer, this.readBuffer, this.onReadCompletion);
            }
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            Logger.info("OnWriteCompletionHandler::failed. Shutdown the channel.");
            ChannelUtil.close(this.client);
        }
    }

    public static class OnReadCompletionHandler implements CompletionHandler<Integer, ByteBuffer> {

        private AsynchronousSocketChannel client;

        public OnReadCompletionHandler(AsynchronousSocketChannel client) {
            this.client = client;
        }

        @Override
        public void completed(Integer result, ByteBuffer buffer) {
            Logger.info("OnReadCompletionHandler::completed");

            if (result == -1) {
                Logger.info("Server has closed the connection, shutdown the channel.");
                ChannelUtil.close(this.client);
            } else {
                buffer.flip();

                if (buffer.hasRemaining()) {
                    // print messages from server
                    byte[] msgBytes = new byte[buffer.remaining()];
                    buffer.get(msgBytes);
                    Logger.printNewMessage(new String(msgBytes).trim());
                }

                buffer.clear();

                if (result <= buffer.capacity()) {
                    Logger.info("Buffer data has been all read out, shutdown the channel.");
                    ChannelUtil.close(this.client);
                } else {
                    Logger.info("Client connection still running, continue the channel.");
                    this.client.read(buffer, buffer, this);
                }
            }
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            Logger.info("OnReadCompletionHandler::failed. Shutdown the channel.");
            ChannelUtil.close(this.client);
        }
    }
}
