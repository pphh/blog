package util;

import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by huangyinhuang on 11/23/2017.
 */
public class Logger {

    public static void info(String msg) {
        String threadName = Thread.currentThread().getName();

        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);

        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }

    public static void printNewMessage(String msg) {
        String info = String.format("[New Message] %s", msg);
        Logger.info(info);
    }

    public static void printClientInfo(SocketChannel client) {
        Logger.info(client.socket().getLocalSocketAddress() + " local --> remote " + client.socket().getRemoteSocketAddress());
    }

    public static void printServerInfo(ServerSocketChannel server) {
        Logger.info(server.socket().getLocalSocketAddress() + " local --> remote n/a");
    }

}
