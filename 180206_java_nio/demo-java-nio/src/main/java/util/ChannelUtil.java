package util;

import java.io.IOException;
import java.nio.channels.Channel;

/**
 * Created by huangyinhuang on 2/2/2018.
 */
public class ChannelUtil {

    public static void close(Channel channel) {
        try {
            channel.close();
        } catch (IOException e) {
            Logger.info("Receive an exception when trying to shutdown the channel.");
        }
    }

}
