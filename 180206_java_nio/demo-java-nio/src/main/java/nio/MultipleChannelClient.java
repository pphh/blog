package nio;

import util.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;

/**
 * Created by huangyinhuang on 2/1/2018.
 */
public class MultipleChannelClient {

    private static List<SocketChannel> channels = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        Selector selector = Selector.open();

        registerNewChannel(selector);
        registerNewChannel(selector);
        registerNewChannel(selector);
        registerNewChannel(selector);

        int count = 0;
        while (true) {
            Logger.info("try to connect server, count = " + count++);
            Thread.sleep(2000);
            if (selector.select() == 0) {
                // send a message to server
                String msg = "Hello server! " + new Random().nextInt();
                sendMessageByRandomChannel(count);
                continue;
            }

            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
            while (keyIterator.hasNext()) {
                SelectionKey key = keyIterator.next();

                if (key.isConnectable()) {
                    // a connection was established with a remote server.
                    Logger.info("a connection was established with a remote server.");
                    SocketChannel clientChannel = (SocketChannel) key.channel();
                    printChannelInfo(clientChannel);

                    if (clientChannel.isConnectionPending()) {
                        clientChannel.finishConnect();
                    }

                    // connected with client
                    clientChannel.configureBlocking(false);
                    clientChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);

                } else if (key.isWritable()) {
                    // a channel is ready for writing
                    Logger.info("a channel is ready for writing.");

                    // send a greeting to server
                    SocketChannel clientChannel = (SocketChannel) key.channel();
                    printChannelInfo(clientChannel);

                    String msg = "Hello server! This is greeting from client." + count;
                    sendMessage(clientChannel, msg);

                } else if (key.isReadable()) {
                    // a channel is ready for reading
                    Logger.info("a channel is ready for reading.");

                    SocketChannel clientChannel = (SocketChannel) key.channel();
                    printChannelInfo(clientChannel);
                    ByteBuffer buffer = ByteBuffer.allocate(64);

                    if (clientChannel.read(buffer) != -1) {
                        buffer.flip();

                        if (buffer.hasRemaining()) {
                            byte[] msgBytes = new byte[buffer.remaining()];
                            buffer.get(msgBytes);
                            String msg = "Message from server: " + new String(msgBytes);
                            Logger.info(msg);
                        }

                        buffer.clear();
                    }
                }

                keyIterator.remove();
            }
        }

//        channel.close();
//        System.out.print("the end.");
    }

    public static void registerNewChannel(Selector selector) throws IOException {
        InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 9000);
        SocketChannel channel = SocketChannel.open(addr);
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_CONNECT);
        channels.add(channel);
    }

    public static void sendMessageByRandomChannel(int random) {
        int index = random % channels.size();
        SocketChannel channel = channels.get(index);

        String msg = "Hello server! Channel " + index;
        sendMessage(channel, msg);
    }

    public static void sendMessage(SocketChannel channel, String msg) {
        Logger.info("Sending message: " + msg);
        ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
        try {
            channel.write(buffer);
        } catch (IOException e) {
            Logger.info("An exception is thrown when trying to send message to server.Details: " + e.getMessage());
        }
    }

    public static void printChannelInfo(SocketChannel channel) {
        Logger.info(channel.socket().getLocalSocketAddress()
                + " local --> remote " + channel.socket().getRemoteSocketAddress());
    }
}
