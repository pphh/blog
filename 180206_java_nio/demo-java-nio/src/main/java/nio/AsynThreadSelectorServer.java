package nio;

import util.ChannelUtil;
import util.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by huangyinhuang on 2/1/2018.
 * connect with socket client with different child thread
 * child thread will not be blocked by client connection
 */
public class AsynThreadSelectorServer {

    private static ExecutorService threadPool;

    public static void main(String[] args) throws Exception {
        int cpu = Runtime.getRuntime().availableProcessors();
        threadPool = Executors.newFixedThreadPool(cpu);

        // please note: configure channel blocking state as false
        Logger.info("start the server.");
        ServerSocketChannel server = ServerSocketChannel.open();
        server.configureBlocking(false);

        ServerSocket serverSocket = server.socket();
        serverSocket.bind(new InetSocketAddress(9000));

        // register the channel
        Selector selector = Selector.open();
        server.register(selector, SelectionKey.OP_ACCEPT);

        // check new connection's events at intervals
        int count = 0;
        while (true) {
            Logger.info("Wait for connection, count = " + count++);
            Thread.sleep(1000);

            // please note: selector.select() will block current thread
            if (selector.select() == 0) {
                continue;
            }

            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
            while (keyIterator.hasNext()) {
                SelectionKey key = keyIterator.next();
                Logger.info("Receive selection key from channel: " + key.channel());

                if (key.isAcceptable()) {
                    // a connection was accepted by a ServerSocketChannel.
                    Logger.info("a connection was accepeted by a ServerSocketChannel.");
                    ServerSocketChannel srvChannel = (ServerSocketChannel) key.channel();
                    Logger.printServerInfo(srvChannel);

                    // connected with client
                    SocketChannel socket = srvChannel.accept();
                    socket.configureBlocking(false);

                    socket.register(selector, SelectionKey.OP_READ);
                } else if (key.isConnectable()) {
                    // a connection was established with a remote server.
                    Logger.info("a connection was established with a remote server.");
                } else if (key.isReadable()) {
                    // a channel is ready for reading
                    Logger.info("a channel is ready for reading.");
                    SocketChannel channel = (SocketChannel) key.channel();
                    Logger.printClientInfo(channel);

                    // handle upcoming new channel message by thread
                    threadPool.submit(new HandlerThread(channel));
                } else if (key.isWritable()) {
                    // a channel is ready for writing
                    Logger.info("a channel is ready for writing.");
                }

                keyIterator.remove();
            }
        }

    }

    public static class HandlerThread implements Runnable {

        private SocketChannel channel;

        public HandlerThread(SocketChannel channel) {
            this.channel = channel;
        }

        @Override
        public void run() {
            Logger.info("try to read the message from client.");

            ByteBuffer buffer = ByteBuffer.allocate(64);
            try {
                if (channel.read(buffer) != -1) {
                    buffer.flip();

                    if (buffer.hasRemaining()) {
                        byte[] msgBytes = new byte[buffer.remaining()];
                        buffer.get(msgBytes);
                        Logger.printNewMessage(new String(msgBytes).trim());

                        // send a reply message
                        String msg = "Hello client, this is server implemented by async-multiple-thread selector.\r\n";
                        channel.write(ByteBuffer.wrap(msg.getBytes()));
                    }

                    buffer.clear();
                } else {
                    Logger.info("Client has closed the connection, shutdown the channel.");
                    ChannelUtil.close(this.channel);
                }
            } catch (IOException e) {
                Logger.info("An exception is thrown when trying to read channel. Details: " + e.getMessage());
                ChannelUtil.close(this.channel);
            }

        }

    }

}
