package nio;

import util.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by huangyinhuang on 1/31/2018.
 * connect with socket client one by one, using current thread
 * current thread will be blocked by client connection
 */
public class SingleThreadSelectorServer {

    public static void main(String[] args) throws Exception {
        // please note: configure channel blocking state as false
        Logger.info("start the server.");
        ServerSocketChannel server = ServerSocketChannel.open();
        server.configureBlocking(false);

        ServerSocket serverSocket = server.socket();
        serverSocket.bind(new InetSocketAddress(9000));

        // register the channel
        Selector selector = Selector.open();
        server.register(selector, SelectionKey.OP_ACCEPT);

        // check new connection's events at intervals
        int count = 0;
        while (true) {
            Logger.info("Wait for connection, count = " + count++);
            Thread.sleep(1000);

            // please note: selector.select() will block current thread
            if (selector.select() == 0) {
                continue;
            }

            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
            while (keyIterator.hasNext()) {
                SelectionKey key = keyIterator.next();
                Logger.info("Receive selection key from channel: " + key.channel());

                if (key.isAcceptable()) {
                    // a connection was accepted by a ServerSocketChannel.
                    Logger.info("a connection was accepted by a ServerSocketChannel.");
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                    Logger.printServerInfo(channel);

                    // connected with client
                    SocketChannel socket = channel.accept();
                    socket.configureBlocking(false);

                    socket.register(selector, SelectionKey.OP_READ);
                } else if (key.isConnectable()) {
                    // a connection was established with a remote server.
                    Logger.info("a connection was established with a remote server.");
                } else if (key.isReadable()) {
                    // a channel is ready for reading
                    Logger.info("a channel is ready for reading.");
                    SocketChannel channel = (SocketChannel) key.channel();
                    Logger.printClientInfo(channel);

                    // handle upcoming channel by current thread
                    readMessageAndReply(channel);
                    channel.close();
                } else if (key.isWritable()) {
                    // a channel is ready for writing
                    Logger.info("a channel is ready for writing.");
                }

                keyIterator.remove();
            }
        }

    }

    public static void readMessageAndReply(SocketChannel channel) throws IOException {
        Logger.info("try to read the message from client.");

        ByteBuffer buffer = ByteBuffer.allocate(64);
        while (channel.read(buffer) != -1) {
            buffer.flip(); // turn to read mode by flip method

            while (buffer.hasRemaining()) {
                System.out.print((char) buffer.get());
            }

            buffer.clear(); // turn to write mode after clear call
        }

        // send reply message
        String msg = "Hello client, this is server implemented by single selector thread.\r\n";
        channel.write(ByteBuffer.wrap(msg.getBytes()));
    }
}
