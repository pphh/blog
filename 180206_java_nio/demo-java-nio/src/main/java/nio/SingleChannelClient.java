package nio;

import util.Logger;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by huangyinhuang on 2/1/2018.
 */
public class SingleChannelClient {

    private static SocketChannel channel;

    public static void main(String[] args) throws Exception {
        InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 9000);
        channel = SocketChannel.open();
        channel.configureBlocking(true);

        channel.connect(addr);
        int count = 0;
        while (!channel.finishConnect()) {
            System.out.print("waiting for server, count = " + count++);
            Thread.sleep(200);
        }
        Logger.info("connection from " + channel.socket().getRemoteSocketAddress());

        // send a message to server
        for (int i = 0; i < 20; i++) {
            String msg = "Hello server! " + i;
            Logger.info("Sending message: " + msg);
            ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
            channel.write(buffer);
            Thread.sleep(1000);
        }

        channel.close();
        System.out.print("the end.");
    }
}
