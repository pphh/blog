# blog

## 1. 说明

博客网站[hyhblog.cn](https://hyhblog.cn)所用的资料和代码，包括，
- PPT资料
- 演示代码文件
- 图片
- 工具
- 等等

当前文件目录下各个子目录各为独立项目，各自运行，互不相关，演示样例尽量简单完整。

如下为各个项目的简单介绍，各个项目的详情介绍/演示运行，请参考博文或相关README.md文档。

* [171019_namespace_cgroup](./171019_namespace_cgroup) - 《[在Linux中使用namespace和cgroup实现进程的资源隔离和限制](http://www.hyhblog.cn/2017/10/18/linux_namespace_cgroup/)》。
* [171027_mesos_intro](./171027_mesos_intro) - 《[Mesos架构和工作流程简介](http://www.hyhblog.cn/2017/10/27/mesos_intro/)》。
* [171117_java_collection](./171117_java_collection) - 《[Java集合框架和各实现类性能测试](http://www.hyhblog.cn/2017/11/17/java_collection_framework/)》。
* [171124_java_stream](./171124_java_stream) - 《[Java Stream简介和使用参考](http://www.hyhblog.cn/2017/11/27/java_steam_intro/)》。
* [171201_java_object_copy_convert](./171201_java_object_copy_convert) - 《[Java对象的类型转换和属性复制](http://www.hyhblog.cn/2017/12/04/java_object_copy_convert/)》。
* [171221_java_compile_run](./171221_java_compile_run) - 《[如何使用Javac/Jar/Java工具对源代码进行编译打包执行](http://www.hyhblog.cn/2017/12/22/java_compile_package_run/)》。
* [180206_java_nio](./180206_java_nio) - 《[Java NIO的核心组件简介和使用](http://www.hyhblog.cn/2018/02/13/java-nio-intro/)》。
* [180830_java_concurrency](./180830_java_concurrency) - 《[从类状态看Java多线程安全并发](http://www.hyhblog.cn/2018/09/16/java_safe_concurrency/)》。
* [210215_k8s_deployment](./210215_k8s_deployment) - 《[新手指南：k8s集群单机部署](http://www.hyhblog.cn/2021/02/17/deployment-manual-k8s-for-newbee/)》。


更多博文演示项目见[simple-demo](https://gitee.com/pphh/simple-demo)。

## 2. 联系 Contact

邮箱地址：peipeihh@qq.com，欢迎来信联系。

更多的信息，可以访问博客地址：[hyhblog.cn](https://hyhblog.cn)。

## 3. 知识共享
博客博文和相关资料使用[知识共享2.5中国大陆版 许可协议(CC BY-NC-SA 2.5 CN)](https://creativecommons.org/licenses/by-nc-sa/2.5/cn/)，即：署名-非商业性使用-相同方式共享。

相关演示代码文件遵从Apache License 2.0的开源许可协议。
