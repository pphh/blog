package com.pphh.demo;


import com.pphh.demo.shared.Logger;
import com.pphh.demo.stream.SimpleStream;
import com.pphh.demo.stream.StreamBuilder;
import com.pphh.demo.stream.StreamOperations;
import com.pphh.demo.stream.StreamParallel;

public class Demo {

    public static void main(String[] args) {
        Logger.logMessage("test");

        // 演示几个简单的stream集合处理
        SimpleStream.demo();
        SimpleStream.demo2();
        SimpleStream.demo3();

        // 演示如何创建Stream
        StreamBuilder.demo();

        // 演示Stream的中间操作方法(intermediate opertions)
        StreamOperations.doFilter();
        StreamOperations.doMap();
        StreamOperations.doForEach();
        StreamOperations.doPeek();
        StreamOperations.doReduce();
        StreamOperations.doSkip();
        StreamOperations.doLimit();
        StreamOperations.doSort();
        StreamOperations.doSortWithLimit();
        StreamOperations.doMinMax();
        StreamOperations.doDistinct();
        StreamOperations.doMatch();
        StreamOperations.doIterate();
        StreamOperations.doCollectors();

        // 演示Stream的串并行处理
        StreamParallel.runWithoutParallel();
        StreamParallel.runWithParallel();
        StreamParallel.runWithParallelEx();

    }

}
