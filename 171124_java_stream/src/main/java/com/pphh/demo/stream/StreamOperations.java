package com.pphh.demo.stream;

import com.pphh.demo.shared.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by huangyinhuang on 11/24/2017.
 */
public class StreamOperations {

    public static void doFilter() {
        Logger.logMessage("演示：Stream.filter方法");

        Logger.logMessage("获取偶数");
        Integer[] sixNums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Integer[] evens = Stream.of(sixNums)
                .filter(n -> n % 2 == 0)
                .toArray(Integer[]::new);
        Logger.logMessage(evens.toString());

        Logger.logMessage("挑出字符串长度大于5的用户名");
        String[] users = new String[]{"michael", "jelen", "jack"};
        List<String> result = Stream.of(users)
                .filter(username -> username.length() > 5)
                .collect(Collectors.toList());
        Logger.logMessage(result.toString());
    }

    public static void doMap() {
        Logger.logMessage("演示：Stream.map方法");

        Logger.logMessage("对用户名进行字符串大写处理");
        String[] users = new String[]{"michael", "jelen", "jack"};
        List<String> result = Stream.of(users)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        Logger.logMessage(result.toString());

        Logger.logMessage("对数字进行平方处理");
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<Integer> result2 = numbers.stream()
                .map(n -> n * n)
                .collect(Collectors.toList());
        Logger.logMessage(result2.toString());

        Logger.logMessage("对多个集合进行合并，一对多的map处理");
        Stream<List<Integer>> inputStream = Stream.of(
                Arrays.asList(1),
                Arrays.asList(2, 3),
                Arrays.asList(4, 5, 6)
        );
        List<Integer> result3 = inputStream
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        Logger.logMessage(result3.toString());
    }

    public static void doForEach() {
        Logger.logMessage("演示：Stream.forEach方法");
        IntStream.range(0, 10).forEach(Logger::logMessage);
    }

    public static void doPeek() {
        Logger.logMessage("演示：Stream.peek方法");
        IntStream.range(0, 10)
                .filter(n -> n % 2 == 0)
                .peek(Logger::logMessage)
                .map(n -> n * 2)
                .peek(Logger::logMessage)
                .sum();
    }

    public static void doReduce() {
        Logger.logMessage("演示：Stream.reduce方法");
        Integer sum1 = IntStream.range(0, 10)
                .filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .reduce(0, Integer::sum);
        Logger.logMessage("sum1 = " + sum1);

        Integer sum2 = IntStream.range(0, 10)
                .filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .reduce(0, (x, y) -> x + y);
        Logger.logMessage("sum2 = " + sum2);
    }


    public static void doSkip() {
        Logger.logMessage("演示：Stream.skip方法");
        List<Integer> result = IntStream.range(1, 10)
                .skip(3)
                .boxed()
                .collect(Collectors.toList());
        Logger.logMessage("获取数列（跳过数组前三个）：" + result);
    }

    public static void doLimit() {
        Logger.logMessage("演示：Stream.limit方法");
        List<Integer> result = IntStream.range(1, 10)
                .limit(3)
                .boxed()
                .collect(Collectors.toList());
        Logger.logMessage("获取数组前三个的数列：" + result);
    }

    public static void doSort() {
        Logger.logMessage("演示：Stream.limit方法");
        Integer[] integers = {1, 2, 3, 4, 5};
        List<Integer> result = Stream.of(integers)
                .sorted((p1, p2) -> p2.compareTo(p1))
                .collect(Collectors.toList());
        Logger.logMessage("数列（按大小倒序排列）： " + result);
    }

    public static void doSortWithLimit() {
        Logger.logMessage("演示：Stream.limit对Sort的影响");

        List<Integer> result = IntStream.range(1, 6)
                .peek(Logger::logMessage)
                .sorted()
                .boxed()
                .collect(Collectors.toList());
        Logger.logMessage("result = " + result);

        List<Integer> result2 = IntStream.range(1, 6)
                .peek(Logger::logMessage)
                .sorted()
                .limit(3)
                .boxed()
                .collect(Collectors.toList());
        Logger.logMessage("result2 = " + result2);
    }

    public static void doMinMax() {
        Logger.logMessage("演示：Stream.min和Stream.max方法");
        Integer max = IntStream.range(1, 6)
                .max()
                .getAsInt();
        Logger.logMessage("max = " + max);

        Integer min = IntStream.range(1, 6)
                .min()
                .getAsInt();
        Logger.logMessage("min = " + min);
    }

    public static void doDistinct() {
        Logger.logMessage("演示：Stream.distinct方法");

        Integer[] numbers = {1, 2, 3, 4, 4, 5, 5};
        List<Integer> result = Stream.of(numbers)
                .distinct()
                .collect(Collectors.toList());
        Logger.logMessage("获取无重复数字的数列：" + result);
    }

    public static void doMatch() {
        Logger.logMessage("演示：Stream.allMatch, anyMatch, noneMatch方法 ");
        Integer[] numbers = {1, 2, 3, 4, 4, 5, 5};
        Boolean allMatched = Stream.of(numbers).allMatch(p -> (p > 0));
        Logger.logMessage("所有数字大于0：" + allMatched);

        Boolean anyMatch = Stream.of(numbers).anyMatch(p -> (p > 0));
        Logger.logMessage("有数字大于0：" + anyMatch);

        Boolean noneMatch = Stream.of(numbers).noneMatch(p -> (p > 0));
        Logger.logMessage("没有数字大于0：" + noneMatch);
    }

    public static void doIterate() {
        Logger.logMessage("演示：Stream.iterate方法 ");
        List list = Stream.iterate(0, n -> n + 2)
                .limit(10)
                .collect(Collectors.toList());
        Logger.logMessage("等差数列：" + list);
    }

    public static void doCollectors() {
        Logger.logMessage("演示：Stream.Collectors方法 ");
        Integer[] numbers = {1, 2, 3, 4, 4, 5, 5};

        // 将数组中的数据按值进行分组
        Map<Integer, List<Integer>> result = Stream.of(numbers)
                .collect(Collectors.groupingBy(p -> p));
        for (Object o : result.entrySet()) {
            Map.Entry<Integer, List<Integer>> entry = (Map.Entry) o;
            Logger.logMessage("Group " + entry.getKey() + ": " + entry.getValue().toString());
        }

        // 将数组中的数据分成两组：小于2，大于等于2
        Map<Boolean, List<Integer>> result2 = Stream.of(numbers)
                .collect(Collectors.partitioningBy(p -> p < 2));
        Logger.logMessage("小于2的：" + result2.get(Boolean.TRUE).toString());
        Logger.logMessage("大于等于2的：" + result2.get(Boolean.FALSE).toString());
    }

}
