package com.pphh.demo.stream;

import com.pphh.demo.shared.Logger;

import java.util.stream.IntStream;

/**
 * Created by huangyinhuang on 11/24/2017.
 */
public class StreamParallel {

    public static void runWithoutParallel() {
        Logger.logMessage("演示：串行执行数据流式处理，注意观察数列输出顺序");

        IntStream.range(0, 10)
                .peek(Logger::logMessage)
                .toArray();
    }

    public static void runWithParallel() {
        Logger.logMessage("演示：并行执行数据流式处理，注意观察数列输出顺序");

        IntStream.range(0, 10)
                .parallel()
                .peek(Logger::logMessage)
                .toArray();
    }

    public static void runWithParallelEx() {
        Logger.logMessage("演示：并行执行数据流式处理, peek被执行两次，注意观察数列输出顺序");

        IntStream.range(0, 10)
                .parallel()
                .peek(Logger::logMessage)
                .peek(Logger::logMessage)
                .toArray();
    }

}
