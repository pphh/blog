package com.pphh.demo.stream;

import com.pphh.demo.shared.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by huangyinhuang on 11/24/2017.
 */
public class StreamBuilder {

    public static void demo() {

        // 1. Arrays
        Logger.logMessage("演示：通过Java数组生成stream对象");
        String[] strArray = new String[]{"a", "b", "c"};
        Stream stream1 = Stream.of(strArray);
        Stream stream2 = Arrays.stream(strArray);

        printStreamInfo("stream1", stream1);
        printStreamInfo("stream2", stream2);

        // 2. Collection
        Logger.logMessage("演示：通过Java Collection类生成stream对象");
        List<String> list = Arrays.asList(strArray);
        Stream stream3 = list.stream();

        printStreamInfo("stream3", stream3);

        // 3. Stream Factory Method
        Logger.logMessage("演示：通过Stream静态工厂类生成stream对象");
        Stream stream4 = Stream.of("a", "b", "c");
        IntStream intStream = IntStream.range(0, 10);

        printStreamInfo("stream4", stream4);
        printStreamInfo("stream5", intStream);

        Random seed = new Random();
        Supplier<Integer> random = seed::nextInt;
        Stream stream6 = Stream.generate(random).limit(10);
        printStreamInfo("stream6-随机数", stream6);
    }

    public static void printStreamInfo(String streamName, Stream stream) {
        Logger.logMessage(streamName + "的集合元素信息");
        stream.forEach(Logger::logMessage);
    }

    public static void printStreamInfo(String streamName, IntStream stream) {
        Logger.logMessage(streamName + "的集合元素信息");
        stream.forEach(Logger::logMessage);
    }
}
