package com.pphh.demo.stream;

import com.pphh.demo.shared.Logger;

import java.util.stream.IntStream;

/**
 * Created by huangyinhuang on 11/24/2017.
 * <p>
 * 演示filter-map-reduce操作
 */
public class SimpleStream {

    public static void demo() {
        Logger.logMessage("演示：简单的filter-map-reduce操作");
        Integer sum = IntStream.range(0, 10)
                .filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .reduce(0, (x, y) -> x + y);
        Logger.logMessage("result = " + sum);
    }

    public static void demo2() {
        Logger.logMessage("演示：简单的filter-map-reduce操作，使用Integer::sum");
        Integer sum = IntStream.range(0, 10)
                .filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .reduce(0, Integer::sum);
        Logger.logMessage("result = " + sum);
    }

    public static void demo3() {
        Logger.logMessage("演示：简单的filter-map-reduce操作, sum是一种reduce操作");
        Integer sum = IntStream.range(0, 10)
                .filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .sum();
        Logger.logMessage("result = " + sum);
    }

}
