package com.pphh.demo.shared;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    public static void logMessage(Object msg) {
        String threadName = Thread.currentThread().getName();

        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);

        String info = String.format("[%s][%s] %s", strTime, threadName, msg.toString());
        System.out.println(info);
    }

}
