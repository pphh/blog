package com.pphh;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public interface TestCase {

    public String getDescription();
    public void run() throws Exception;

}
