package com.pphh.map;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public enum MapType {
    HASH_MAP,
    LINKED_HASH_MAP,
    TREE_MAP,
    WEAK_HASH_MAP,
    HASH_TABLE
}
