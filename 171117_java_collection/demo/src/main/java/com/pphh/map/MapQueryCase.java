package com.pphh.map;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.Map;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class MapQueryCase implements TestCase {

    private MapType mapType;
    private long mapSize = 0;
    private Map map;
    private CheckPoint checkPoint;

    public MapQueryCase(MapType mapType, Map map, CheckPoint checkPoint) {
        this.mapType = mapType;
        this.map = map;
        this.mapSize = map.size();
        this.checkPoint = checkPoint;
    }

    @Override
    public String getDescription() {
        return mapType.name() + "的" + checkPoint.name() + "查询性能";
    }

    @Override
    public void run() throws Exception {
        if (map == null) {
            throw new Exception("map没有初始化，请先进行初始化操作");
        }

        // 轮询查询数组中的是否含有0到mapSize的变量
        long matchedCount = 0;
        for (long i = mapSize; i >= 0; i--) {
            switch (checkPoint) {
                case KEY:
                    if (map.containsKey(i)) matchedCount++;
                    break;
                case VALUE:
                    if (map.containsValue(i)) matchedCount++;
                    break;
            }
        }

        Demo.logMessage("匹配个数为：" + matchedCount);
    }

    public Map getResult() {
        return map;
    }

    public enum CheckPoint {
        KEY,
        VALUE
    }
}
