package com.pphh.map;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.Map;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class MapRemoveCase implements TestCase {

    private MapType mapType;
    private long mapSize = 0;
    private Map map;

    public MapRemoveCase(MapType mapType, Map map) {
        this.mapType = mapType;
        this.map = map;
        this.mapSize = map.size();
    }

    @Override
    public String getDescription() {
        return mapType.name() + "的删除性能";
    }

    @Override
    public void run() throws Exception {
        if (map == null) {
            throw new Exception("map没有初始化，请先进行初始化操作");
        }

        // 轮询查询数组中的是否含有0到mapSize的变量
        long matchedCount = 0;
        for (long i = mapSize; i >= 0; i--) {
            Object value = map.remove(i);
            if (value != null) matchedCount++;
        }

        Demo.logMessage("匹配个数为：" + matchedCount);
    }

    public Map getResult() {
        return map;
    }


}
