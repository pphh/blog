package com.pphh.map;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.*;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class MapInsertCase implements TestCase {

    private MapType mapType;
    private long mapSize = 0;
    private Map map;

    public MapInsertCase(MapType mapType, long mapSize) {
        this.mapType = mapType;
        this.mapSize = mapSize;

        switch (mapType) {
            case HASH_MAP:
                this.map = new HashMap();
                break;
            case LINKED_HASH_MAP:
                this.map = new LinkedHashMap();
                break;
            case TREE_MAP:
                this.map = new TreeMap();
                break;
            case WEAK_HASH_MAP:
                this.map = new WeakHashMap();
                break;
        }
    }

    @Override
    public String getDescription() {
        return mapType.name() + "的插入性能";
    }

    @Override
    public void run() throws Exception {
        if (map == null) {
            throw new Exception("map没有初始化，请先进行初始化操作");
        }

        long matchedCount = 0;
        for (long i = 0; i < mapSize; i++) {
            map.put(i, i);
            matchedCount++;
        }

        Demo.logMessage("添加个数为：" + matchedCount);
    }

    public Map getResult() {
        return this.map;
    }

}
