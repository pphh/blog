package com.pphh.map;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class SimpleMap implements TestCase {
    @Override
    public String getDescription() {
        return "新建简单的HashMap\\TreeMap\\LinkedHashMap\\WeakHashMap\\Hashtable并打印出数组值，请观察其插入和排列顺序";
    }

    @Override
    public void run() throws Exception {
        Map map1 = new HashMap();
        Map map2 = new TreeMap();
        Map map3 = new LinkedHashMap();
        Map map4 = new WeakHashMap();
        Map map5 = new Hashtable();
        Map map6 = new ConcurrentHashMap();

        for (int i = 0; i < 5; i++) {
            //产生一个随机数，并将其放入map中
            int value = (int) (Math.random() * 100);
            Demo.logMessage("第 " + i + " 次产生的随机数为：" + value);
            map1.put(i, value);
            map2.put(i, value);
            map3.put(i, value);
            map4.put(i, value);
            map5.put(i, value);
            map6.put(i, value);
        }

        Demo.logMessage("index为key，产生的随机数为value");
        Demo.logMessage("HashMap：" + map1);
        Demo.logMessage("TreeMap：" + map2);
        Demo.logMessage("LinkedHashMap：" + map3);
        Demo.logMessage("WeakHashMap：" + map4);
        Demo.logMessage("Hashtable：" + map5);
        Demo.logMessage("ConcurrentHashMap：" + map6);

        map1.clear();
        map2.clear();
        map3.clear();
        map4.clear();
        map5.clear();
        map6.clear();

        for (int i = 0; i < 10; i++) {
            //产生一个随机数，并将其放入map中
            int value = (int) (Math.random() * 100);
            Demo.logMessage("第 " + i + " 次产生的随机数为：" + value);
            if (!map1.containsKey(value)){
                map1.put(value, i);
                map2.put(value, i);
                map3.put(value, i);
                map4.put(value, i);
                map5.put(value, i);
                map6.put(value, i);
            }
        }

        Demo.logMessage("产生的随机数为key，index为value");
        Demo.logMessage("HashMap：" + map1);
        Demo.logMessage("TreeMap：" + map2);
        Demo.logMessage("LinkedHashMap：" + map3);
        Demo.logMessage("WeakHashMap：" + map4);
        Demo.logMessage("Hashtable：" + map5);
        Demo.logMessage("ConcurrentHashMap：" + map5);
    }
}
