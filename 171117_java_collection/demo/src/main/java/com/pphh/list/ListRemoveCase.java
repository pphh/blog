package com.pphh.list;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.List;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class ListRemoveCase implements TestCase {

    private ListType listType;
    private long listSize = 0;
    private List<Long> list;

    public ListRemoveCase(ListType listType, List<Long> list) {
        this.listType = listType;
        this.listSize = list.size();
        this.list = list;
    }

    @Override
    public String getDescription() {
        return listType.name() + "的删除性能";
    }

    @Override
    public void run() throws Exception {
        if (list == null) {
            throw new Exception("数组没有初始化，请先进行初始化操作");
        }

        // 从数组中删除含有0到listSize的变量
        long removedCount = 0;
        for (long i = listSize; i > 0; i--) {
            if (list.remove(i)) removedCount++;
        }
        Demo.logMessage("删除个数为：" + removedCount);
    }

    public List<Long> getResult() {
        return this.list;
    }

}
