package com.pphh.list;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.List;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class ListQueryCase implements TestCase {
    private ListType listType;
    private long listSize = 0;
    private List<Long> list;

    public ListQueryCase(ListType listType,List<Long> list){
        this.listType = listType;
        this.listSize = list.size();
        this.list = list;
    }

    @Override
    public String getDescription() {
        return listType.name() + "的查询性能";
    }

    @Override
    public void run() throws Exception {
        if (list == null) {
            throw new Exception("数组没有初始化，请先进行初始化操作");
        }

        // 轮询查询数组中的是否含有0到listSize的变量
        long matchedCount = 0;
        for (long i = listSize; i > 0; i--) {
            int index = list.indexOf(i);
            if (index >= 0) matchedCount++;
        }
        Demo.logMessage("匹配个数为：" + matchedCount);
    }

    public List<Long> getResult() {
        return this.list;
    }

}
