package com.pphh.list;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.Collection;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class CollectionQueryCase implements TestCase {

    private long arraySize = 0;
    private Collection<Long> array;

    public CollectionQueryCase(Collection<Long> testArray) {
        this.init(testArray);
    }

    @Override
    public String getDescription() {
        return "Collection的查询性能";
    }

    @Override
    public void run() throws Exception {
        if (array == null) {
            throw new Exception("数组没有初始化，请先进行初始化操作");
        }

        // 轮询查询数组中的是否含有0到arraysize的变量
        long matchedCount = 0;
        for (long i = arraySize; i > 0; i--) {
            if (array.contains(i)) matchedCount++;
        }
        Demo.logMessage("匹配个数为：" + matchedCount);
    }

    public void init(Collection<Long> testArray) {
        this.arraySize = testArray.size();
        this.array = testArray;
    }

    public Collection<Long> getResult() {
        return array;
    }
}
