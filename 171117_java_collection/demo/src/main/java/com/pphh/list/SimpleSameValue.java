package com.pphh.list;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.*;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class SimpleSameValue implements TestCase {

    @Override
    public String getDescription() {
        return "插入相同的值进入ArrayList\\LinkedList\\Vector";
    }

    @Override
    public void run() throws Exception {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new LinkedList<>();
        List<Integer> list3 = new Vector<>();

        for (int i = 0; i < 10; i++) {
            //每次插入相同的值
            list1.add(10);
            list2.add(10);
            list3.add(10);
        }

        Demo.logMessage("ArrayList：" + list1);
        Demo.logMessage("LinkedList：" + list2);
        Demo.logMessage("Vector：" + list3);
    }
}
