package com.pphh.list;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class ListInsertCase implements TestCase {

    private ListType listType;
    private long listSize = 0;
    private List<Long> list;

    public ListInsertCase(ListType listType, long listSize){
        this.listType = listType;
        this.listSize = listSize;
        switch (listType){
            case ARRAY_LIST:
                this.list = new ArrayList<Long>();
                break;
            case LINKED_LIST:
                this.list = new LinkedList<Long>();
                break;
            case  VECTOR:
                this.list = new Vector<Long>();
                break;
        }
    }

    @Override
    public String getDescription() {
        return listType.name() + "的插入性能";
    }

    @Override
    public void run() throws Exception {
        if (list == null) {
            throw new Exception("数组没有初始化，请先进行初始化操作");
        }

        // 在数组中第一个位置不断插入变量，一直到数组指定测试大小
        long matchedCount = 0;
        for (long i = listSize; i > 0; i--) {
            list.add(0, i);
            matchedCount++;
        }

        Demo.logMessage("添加个数为：" + matchedCount);
    }

    public List<Long> getResult() {
        return this.list;
    }

}
