package com.pphh.list;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.*;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class SimpleList implements TestCase {

    @Override
    public String getDescription() {
        return "新建简单的ArrayList\\LinkedList\\Vector并打印出数组值，请观察其插入和排列顺序";
    }

    @Override
    public void run() throws Exception {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new LinkedList<>();
        List<Integer> list3 = new Vector<>();

        for (int i = 0; i < 10; i++) {
            //产生一个随机数，并将其放入List中
            int value = (int) (Math.random() * 100);
            Demo.logMessage("第 " + i + " 次产生的随机数为：" + value);
            list1.add(value);
            list2.add(value);
            list3.add(value);
        }

        Demo.logMessage("ArrayList：" + list1);
        Demo.logMessage("LinkedList：" + list2);
        Demo.logMessage("Vector：" + list3);

    }
}
