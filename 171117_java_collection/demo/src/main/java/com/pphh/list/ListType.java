package com.pphh.list;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public enum ListType {
    ARRAY_LIST,
    LINKED_LIST,
    VECTOR
}
