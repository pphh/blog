package com.pphh;

import com.pphh.list.*;
import com.pphh.map.*;
import com.pphh.set.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Demo app - Java collection framework
 */
public class Demo {

    public static void main(String[] args) {

        TestRunner testRunner = new TestRunner();
        testRunner.execute(new SimpleList());
        testRunner.execute(new SimpleSet());
        testRunner.execute(new com.pphh.list.SimpleSameValue());
        testRunner.execute(new com.pphh.set.SimpleSameValue());
        testRunner.execute(new SimpleMap());

        runListPerformanceTest(ListType.ARRAY_LIST, 10000);
        runListPerformanceTest(ListType.ARRAY_LIST, 50000);
        runListPerformanceTest(ListType.ARRAY_LIST, 100000);
        runListPerformanceTest(ListType.ARRAY_LIST, 150000);

        runListPerformanceTest(ListType.LINKED_LIST, 10000);
        runListPerformanceTest(ListType.LINKED_LIST, 50000);
        runListPerformanceTest(ListType.LINKED_LIST, 100000);
        runListPerformanceTest(ListType.LINKED_LIST, 150000);

        runListPerformanceTest(ListType.VECTOR, 10000);
        runListPerformanceTest(ListType.VECTOR, 50000);
        runListPerformanceTest(ListType.VECTOR, 100000);
        runListPerformanceTest(ListType.VECTOR, 150000);

        runMapPerformanceTest(MapType.HASH_MAP, 10000);
        runMapPerformanceTest(MapType.HASH_MAP, 50000);
        runMapPerformanceTest(MapType.HASH_MAP, 100000);
        runMapPerformanceTest(MapType.HASH_MAP, 150000);

        runMapPerformanceTest(MapType.LINKED_HASH_MAP, 10000);
        runMapPerformanceTest(MapType.LINKED_HASH_MAP, 50000);
        runMapPerformanceTest(MapType.LINKED_HASH_MAP, 100000);
        runMapPerformanceTest(MapType.LINKED_HASH_MAP, 150000);

        runMapPerformanceTest(MapType.TREE_MAP, 10000);
        runMapPerformanceTest(MapType.TREE_MAP, 50000);
        runMapPerformanceTest(MapType.TREE_MAP, 100000);
        runMapPerformanceTest(MapType.TREE_MAP, 150000);

        runMapPerformanceTest(MapType.WEAK_HASH_MAP, 10000);
        runMapPerformanceTest(MapType.WEAK_HASH_MAP, 50000);
        runMapPerformanceTest(MapType.WEAK_HASH_MAP, 100000);
        runMapPerformanceTest(MapType.WEAK_HASH_MAP, 150000);


        runSetPerformanceTest(SetType.HASH_SET, 10000);
        runSetPerformanceTest(SetType.HASH_SET, 50000);
        runSetPerformanceTest(SetType.HASH_SET, 100000);
        runSetPerformanceTest(SetType.HASH_SET, 150000);
        runSetPerformanceTest(SetType.HASH_SET, 1000000);

        runSetPerformanceTest(SetType.LINKED_HASH_SET, 10000);
        runSetPerformanceTest(SetType.LINKED_HASH_SET, 50000);
        runSetPerformanceTest(SetType.LINKED_HASH_SET, 100000);
        runSetPerformanceTest(SetType.LINKED_HASH_SET, 150000);
        runSetPerformanceTest(SetType.LINKED_HASH_SET, 1000000);

        runSetPerformanceTest(SetType.TREE_SET, 10000);
        runSetPerformanceTest(SetType.TREE_SET, 50000);
        runSetPerformanceTest(SetType.TREE_SET, 100000);
        runSetPerformanceTest(SetType.TREE_SET, 150000);
    }

    public static void runListPerformanceTest(ListType listType, long listSize) {
        logMessage("== 测试" + listType.name() + "大小为" + listSize + "时的性能 ==");
        PerformanceRunner perfRunner = new PerformanceRunner();

        ListInsertCase listInsertCase = new ListInsertCase(listType, listSize);
        perfRunner.execute(listInsertCase);

        List<Long> list = listInsertCase.getResult();
        ListQueryCase listQueryCase = new ListQueryCase(listType, list);
        perfRunner.execute(listQueryCase);

        CollectionQueryCase collectionQueryCase = new CollectionQueryCase(list);
        perfRunner.execute(collectionQueryCase);

        ListRemoveCase listRemoveCase = new ListRemoveCase(listType, list);
        perfRunner.execute(listRemoveCase);

    }

    public static void runMapPerformanceTest(MapType mapType, long mapSize) {
        logMessage("== 测试" + mapType.name() + "大小为" + mapSize + "时的性能 ==");
        PerformanceRunner perfRunner = new PerformanceRunner();

        MapInsertCase mapInsertCase = new MapInsertCase(mapType, mapSize);
        perfRunner.execute(mapInsertCase);

        Map map = mapInsertCase.getResult();
        MapQueryCase mapQueryKeyCase = new MapQueryCase(mapType, map, MapQueryCase.CheckPoint.KEY);
        perfRunner.execute(mapQueryKeyCase);

        MapQueryCase mapQueryValueCase = new MapQueryCase(mapType, map, MapQueryCase.CheckPoint.VALUE);
        perfRunner.execute(mapQueryValueCase);

        MapRemoveCase mapRemoveCase = new MapRemoveCase(mapType, map);
        perfRunner.execute(mapRemoveCase);
    }

    public static void runSetPerformanceTest(SetType setType, long setSize) {
        logMessage("== 测试" + setType.name() + "大小为" + setSize + "时的性能 ==");
        PerformanceRunner perfRunner = new PerformanceRunner();

        SetInsertCase setInsertCase = new SetInsertCase(setType, setSize);
        perfRunner.execute(setInsertCase);

        Set set = setInsertCase.getResult();
        SetQueryCase setQueryCase = new SetQueryCase(setType, set);
        perfRunner.execute(setQueryCase);

        SetRemoveCase setRemoveCase = new SetRemoveCase(setType, set);
        perfRunner.execute(setRemoveCase);
    }

    public static void logMessage(String msg) {
        String threadName = Thread.currentThread().getName();

        Date nowTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss-SSS");
        String strTime = sdFormatter.format(nowTime);

        String info = String.format("[%s][%s] %s", strTime, threadName, msg);
        System.out.println(info);
    }

}
