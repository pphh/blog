package com.pphh;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class TestRunner {


    public void execute(TestCase testCase) {
        Demo.logMessage("== 测试项目：" + testCase.getDescription());

        // run the performance test
        try {
            testCase.run();
        } catch (Exception e) {
            Demo.logMessage("测试过程发现异常:" + e.getMessage());
        }

        Demo.logMessage("");
    }

}
