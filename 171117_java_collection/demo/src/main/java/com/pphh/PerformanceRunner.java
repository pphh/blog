package com.pphh;

import java.time.Duration;
import java.time.Instant;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class PerformanceRunner {

    private Instant startTime = null;
    private Instant endTime = null;

    public void execute(TestCase testCase) {
        Demo.logMessage("测试项目：" + testCase.getDescription());

        // run the performance test
        try {
            logStart();
            testCase.run();
            logEnd();

            Duration timeElapsed = Duration.between(startTime, endTime);
            Demo.logMessage("执行时间为：" + timeElapsed.toMillis() + "毫秒");
        } catch (Exception e) {
            Demo.logMessage("测试过程发现异常:" + e.getMessage());
        }

        Demo.logMessage("");
    }

    public void logStart() {
        this.startTime = Instant.now();
    }

    public void logEnd() {
        if (this.startTime == null) {
            this.logStart();
        }
        this.endTime = Instant.now();
    }
}
