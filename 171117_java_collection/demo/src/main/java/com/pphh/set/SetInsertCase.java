package com.pphh.set;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.*;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class SetInsertCase  implements TestCase {
    private SetType setType;
    private long setSize = 0;
    private Set set;

    public SetInsertCase(SetType setType, long setSize) {
        this.setType = setType;
        this.setSize = setSize;

        switch (setType) {
            case HASH_SET:
                this.set = new HashSet();
                break;
            case LINKED_HASH_SET:
                this.set = new LinkedHashSet();
                break;
            case TREE_SET:
                this.set = new TreeSet();
                break;
        }
    }

    @Override
    public String getDescription() {
        return setType.name() + "的插入性能";
    }

    @Override
    public void run() throws Exception {
        if (set == null) {
            throw new Exception("set没有初始化，请先进行初始化操作");
        }

        long matchedCount = 0;
        for (long i = 0; i < setSize; i++) {
            set.add(i);
            matchedCount++;
        }

        Demo.logMessage("添加个数为：" + matchedCount);
    }

    public Set getResult() {
        return this.set;
    }
}
