package com.pphh.set;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class SimpleSet implements TestCase {

    @Override
    public String getDescription() {
        return "新建简单的HashSet\\LinkedHashSet\\TreeSet并打印出数组值，请观察其插入和排列顺序";
    }

    // 测试HashSet\LinkedHashSet\TreeSet的数组有序性
    // HashSet：无序
    // LinkedHashSet：按插入顺序
    // TreeSet：有序（按大小）
    @Override
    public void run() throws Exception {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new LinkedHashSet<>();
        Set<Integer> set3 = new TreeSet<>();

        for (int i = 0; i < 10; i++) {
            //产生一个随机数，并将其放入Set中
            int value = (int) (Math.random() * 100);
            Demo.logMessage("第 " + i + " 次产生的随机数为：" + value);
            set1.add(value);
            set2.add(value);
            set3.add(value);
        }

        Demo.logMessage("HashSet：" + set1);
        Demo.logMessage("LinkedHashSet：" + set2);
        Demo.logMessage("TreeSet ：" + set3);
    }

}
