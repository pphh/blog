package com.pphh.set;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.Set;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public class SetRemoveCase implements TestCase {

    private SetType setType;
    private long setSize = 0;
    private Set set;

    public SetRemoveCase(SetType setType, Set set) {
        this.setType = setType;
        this.setSize = set.size();
        this.set = set;
    }

    @Override
    public String getDescription() {
        return setType.name() + "的删除性能";
    }

    @Override
    public void run() throws Exception {
        if (set == null) {
            throw new Exception("set没有初始化，请先进行初始化操作");
        }

        long matchedCount = 0;
        for (long i = 0; i < setSize; i++) {
            if (set.remove(i)) matchedCount++;
        }

        Demo.logMessage("删除个数为：" + matchedCount);
    }

    public Set getResult() {
        return this.set;
    }

}
