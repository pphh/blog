package com.pphh.set;

import com.pphh.Demo;
import com.pphh.TestCase;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by huangyinhuang on 11/15/2017.
 */
public class SimpleSameValue implements TestCase {
    @Override
    public String getDescription() {
        return "插入相同的值进入HashSet\\LinkedHashSet\\TreeSet";
    }

    @Override
    public void run() throws Exception {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new LinkedHashSet<>();
        Set<Integer> set3 = new TreeSet<>();

        for (int i = 0; i < 10; i++) {
            //每次插入相同的值
            set1.add(10);
            set2.add(10);
            set3.add(10);
        }

        Demo.logMessage("HashSet：" + set1);
        Demo.logMessage("LinkedHashSet：" + set2);
        Demo.logMessage("TreeSet ：" + set3);
    }
}
