package com.pphh.set;

/**
 * Created by huangyinhuang on 11/16/2017.
 */
public enum SetType {

    HASH_SET,
    LINKED_HASH_SET,
    TREE_SET

}
